#!/usr/bin/env bash
set -e
SCRIPT_DIR="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")"
ROOT_DIR="$(readlink -f "${SCRIPT_DIR}/../..")"
source "${ROOT_DIR}/function.log.sh"
source "${ROOT_DIR}/env.sh"

INSTALLS_DIR="${SYSTEM_INSTALLS}/aws-iam-authenticator"
VERSION_FILE="${INSTALLS_DIR}/version"

function ensure_aws_iam_authenticator_is_installed_from_source () {
  # https://github.com/kubernetes-sigs/aws-iam-authenticator
  local build_dir="${SCRIPT_DIR}/build"
  if [ ! -d "${build_dir}" ]; then
      git clone https://github.com/kubernetes-sigs/aws-iam-authenticator.git "${build_dir}"
  fi
  (
    cd "${build_dir}" && git reset --hard HEAD && git checkout master && git pull
    local version="$(git tag --sort=-version:refname | grep "^v[0-9\.]\+$" | head -n 1)"
    if [ ! -f "${VERSION_FILE}" ] || [[ ! $(cat "${VERSION_FILE}") =~ ^${version}$ ]]; then
      log "INFO" "Install AWS IAM Authenticator plugin ${version} to ${INSTALLS_DIR}"
      git checkout "${version}"
      git clean -d -f -f -x
      rm -rf "${INSTALLS_DIR:?}" || true
      find "${SYSTEM_INSTALLS_BIN}/" -xtype l -delete
      mkdir -p "${INSTALLS_DIR}"/bin

      # build binary
      make bin
      # install binary
      cp _output/bin/aws-iam-authenticator "${INSTALLS_DIR}"/bin/aws-iam-authenticator
      cp --archive --recursive --symbolic-link "${INSTALLS_DIR}"/bin/* "${SYSTEM_INSTALLS_BIN}"

      # finalize      
      restorecon -RvF "${SYSTEM_INSTALLS}"
      echo "${version}" >| "${VERSION_FILE}"

      # cleanup
      git clean -d -f -f -x
    fi
  )
}

if [ -d "${SYSTEM_INSTALLS}" ]; then
  ensure_aws_iam_authenticator_is_installed_from_source
fi
