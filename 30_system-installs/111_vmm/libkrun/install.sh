#!/usr/bin/env bash
set -e
SCRIPT_DIR="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")"
ROOT_DIR="$(readlink -f "${SCRIPT_DIR}/../../..")"
source "${ROOT_DIR}/function.log.sh"
source "${ROOT_DIR}/env.sh"

BUILD_DIR="${SCRIPT_DIR}/build"
INSTALLS_DIR="${SYSTEM_INSTALLS}/libkrun"
VERSION_FILE_PREFIX="${INSTALLS_DIR}/version_"

REPO_UPDATED_FILE="${BUILD_DIR}/updated"

function ensure_libkrunfw_is_installed () {
  # https://github.com/containers/libkrunfw
  # requires: python3-pyelftools flex
  local name="libkrunfw"
  local build_dir="${BUILD_DIR}/${name}"
  local version_file="${VERSION_FILE_PREFIX}${name}"
  if [ ! -d "${build_dir}" ]; then
      git clone https://github.com/containers/libkrunfw.git "${build_dir}"
  fi
  pushd "${build_dir}" && git reset --hard HEAD && git checkout main && git remote prune origin && git pull
  #local version="$(git tag --sort=-version:refname | grep "^v[0-9\.]\+$" | head -n 1)"
  local version="$(git rev-parse --short=7 HEAD)"
  if [ ! -f "${version_file}" ] || [[ ! $(cat "${version_file}") =~ ^${version}$ ]]; then
    log "INFO" "Install ${name} ${version} to ${INSTALLS_DIR}"
    git checkout ${version}
    git clean -d -f -f -x

    python -m venv .venv
    source .venv/bin/activate
    pip install pyelftools

    make PREFIX="${INSTALLS_DIR}" -j$(nproc)
    if [ -d "${INSTALLS_DIR}/lib64" ]; then
      rm -f "${INSTALLS_DIR}"/lib64/libkrunfw.*
    fi
    make PREFIX="${INSTALLS_DIR}" install
    chmod -R ug+rwX,o+rX "${INSTALLS_DIR}"

    deactivate

    restorecon -RvF "${SYSTEM_INSTALLS}"
    echo "${version}" >| "${version_file}"
    touch "${REPO_UPDATED_FILE}"
  fi
  popd
}

function ensure_libkrun_is_installed () {
  # https://github.com/containers/libkrun
  # requires: glibc-static patchelf
  local name="libkrun"
  local build_dir="${BUILD_DIR}/${name}"
  local version_file="${VERSION_FILE_PREFIX}${name}"
  if [ ! -d "${build_dir}" ]; then
      git clone https://github.com/containers/libkrun.git "${build_dir}"
  fi
  pushd "${build_dir}" && git reset --hard HEAD && git checkout main && git remote prune origin && git pull
  #local version="$(git tag --sort=-version:refname | grep "^v[0-9\.]\+$" | head -n 1)"
  local version="$(git rev-parse --short=7 HEAD)"
  if  [ -f "${REPO_UPDATED_FILE}" ] || [ ! -f "${version_file}" ] || [[ ! $(cat "${version_file}") =~ ^${version}$ ]]; then
    log "INFO" "Install ${name} ${version} to ${INSTALLS_DIR}"
    git checkout ${version}
    git clean -d -f -f -x
    RUSTFLAGS="-Clink-arg=-Wl,-rpath,${INSTALLS_DIR}/lib64 -L${INSTALLS_DIR}/lib64" make PREFIX="${INSTALLS_DIR}" -j$(nproc)
    make PREFIX="${INSTALLS_DIR}" install
    chmod -R ug+rwX,o+rX "${INSTALLS_DIR}"
    restorecon -RvF "${SYSTEM_INSTALLS}"
    echo "${version}" >| "${version_file}"
    touch "${REPO_UPDATED_FILE}"
  fi
  popd
}

if [ -d "${SYSTEM_INSTALLS}" ]; then
  rm -f "${REPO_UPDATED_FILE}" || true
  ensure_libkrunfw_is_installed
  ensure_libkrun_is_installed
fi
