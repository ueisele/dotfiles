#!/usr/bin/env bash
set -e
SCRIPT_DIR="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")"
ROOT_DIR="$(readlink -f "${SCRIPT_DIR}/..")"
source "${ROOT_DIR}/function.log.sh"
source "${ROOT_DIR}/env.sh"

function ensure_system_installs_are_installed () {
    for tool in $(find "${SCRIPT_DIR}" -regextype posix-extended -regex "^${SCRIPT_DIR}/[^_.][^/]*/system-install\.sh" | sort); do
        log "INFO" "Installing ${tool}"
        ${tool}
    done
}

if [ -z "${SUDO_USER}" ]; then
  echo "The script must be executed with sudo!"
elif [ -d "${SYSTEM_INSTALLS}" ]; then
  ensure_system_installs_are_installed
fi
