#!/usr/bin/env bash
set -e
SCRIPT_DIR="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")"
ROOT_DIR="$(readlink -f "${SCRIPT_DIR}/../..")"
source "${ROOT_DIR}/function.log.sh"
source "${ROOT_DIR}/env.sh"

INSTALLS_DIR="${SYSTEM_INSTALLS}/aws-ssm"
VERSION_FILE="${INSTALLS_DIR}/version"

function ensure_aws_ssm_is_installed_from_source () {
  # https://github.com/aws/session-manager-plugin
  local build_dir="${SCRIPT_DIR}/build"
  if [ ! -d "${build_dir}" ]; then
      git clone https://github.com/aws/session-manager-plugin.git "${build_dir}"
  fi
  (
    cd "${build_dir}" && git reset --hard HEAD && git checkout mainline && git pull
    local version="$(git tag --sort=-version:refname | grep "^[0-9\.]\+$" | head -n 1)"
    if [ ! -f "${VERSION_FILE}" ] || [[ ! $(cat "${VERSION_FILE}") =~ ^${version}$ ]]; then
      log "INFO" "Install AWS Session Manager plugin ${version} to ${INSTALLS_DIR}"
      git checkout "${version}"
      git clean -d -f -f -x
      rm -rf "${INSTALLS_DIR:?}" || true
      find "${SYSTEM_INSTALLS_BIN}/" -xtype l -delete
      mkdir -p "${INSTALLS_DIR}"/bin

      # build binary
      echo -n "${version}" >| VERSION
      go run ./src/version/versiongenerator/version-gen.go
      make GO_BUILD="go build" build-linux-amd64
      # install binary
      cp ./bin/linux_amd64/ssmcli "${INSTALLS_DIR}"/bin/ssmcli
      cp ./bin/linux_amd64_plugin/session-manager-plugin "${INSTALLS_DIR}"/bin/session-manager-plugin
      cp --archive --recursive --symbolic-link "${INSTALLS_DIR}"/bin/* "${SYSTEM_INSTALLS_BIN}"

      # finalize      
      restorecon -RvF "${SYSTEM_INSTALLS}"
      echo "${version}" >| "${VERSION_FILE}"

      # cleanup
      git clean -d -f -f -x
    fi
  )
}

if [ -d "${SYSTEM_INSTALLS}" ]; then
  ensure_aws_ssm_is_installed_from_source
fi
