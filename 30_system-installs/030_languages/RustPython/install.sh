#!/usr/bin/env bash
set -e
SCRIPT_DIR="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")"
ROOT_DIR="$(readlink -f "${SCRIPT_DIR}/../../..")"
source "${ROOT_DIR}/function.log.sh"
source "${ROOT_DIR}/env.sh"

INSTALLS_DIR="${SYSTEM_INSTALLS}/RustPython"
VERSION_FILE="${INSTALLS_DIR}/version"

function ensure_rustpython_is_installed_from_source () {
  # https://github.com/RustPython/RustPython
  log "INFO" "Install RustPython with rust"
  local build_dir="${SCRIPT_DIR}/build"
  if [ ! -d "${build_dir}" ]; then
      git clone https://github.com/RustPython/RustPython.git "${build_dir}"
  fi
  (
    cd "${build_dir}" && git reset --hard HEAD && git checkout main && git pull
    local version="$(git tag --sort=-version:refname | grep "^[0-9\.]\+$" | head -n 1)"
    if [ ! -f "${VERSION_FILE}" ] || [[ ! $(cat "${VERSION_FILE}") =~ ^${version}$ ]]; then
      log "INFO" "Install RustPython ${version} to ${INSTALLS_DIR}"
      git checkout "${version}"
      git clean -d -f -f -x
      rm -rf "${INSTALLS_DIR:?}" || true
      find "${SYSTEM_INSTALLS_BIN}/" -xtype l -delete
      mkdir -p "${INSTALLS_DIR}"/bin

      # build binary
      cargo clean
      cargo build --release --package=rustpython --features=ssl,encodings --locked
      # install binary
      cp ./target/release/rustpython "${INSTALLS_DIR}"/bin/rustpython
      cp --archive --recursive --symbolic-link "${INSTALLS_DIR}"/bin/rustpython "${SYSTEM_INSTALLS_BIN}"

      # build wasm
      cargo clean
      cargo build --target wasm32-wasi --release --package=rustpython --no-default-features --features freeze-stdlib,stdlib --locked
      # install wasm
      cp ./target/wasm32-wasi/release/rustpython.wasm "${INSTALLS_DIR}"/bin/rustpython.wasm

      # finalize      
      chmod -R ug=rwX,o=rX "${INSTALLS_DIR}"
      restorecon -RvF "${SYSTEM_INSTALLS}"
      echo "${version}" >| "${VERSION_FILE}"

      # cleanup
      git clean -d -f -f -x
    fi
  )
}

if [ -d "${SYSTEM_INSTALLS}" ]; then
  ensure_rustpython_is_installed_from_source
fi
