#!/usr/bin/env bash
set -e
SCRIPT_DIR="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")"
ROOT_DIR="$(readlink -f "${SCRIPT_DIR}/../..")"
source "${ROOT_DIR}/function.log.sh"

function ensure_update_script_is_linked () {
    log "INFO" "Linking update script to ${HOME}"
    ln -sf "${SCRIPT_DIR}/install.sh" "${HOME}/.local/bin/update-languages"
}

function ensure_languages_are_installed () {
    for tool in $(find "${SCRIPT_DIR}" -regextype posix-extended -regex "^${SCRIPT_DIR}/[^_.][^/]*/install\.sh" | sort); do
        log "INFO" "Installing ${tool}"
        ${tool}
    done
}

ensure_update_script_is_linked
ensure_languages_are_installed
