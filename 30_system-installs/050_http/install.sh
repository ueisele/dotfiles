#!/usr/bin/env bash
set -e
SCRIPT_DIR="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")"
ROOT_DIR="$(readlink -f "${SCRIPT_DIR}"/../..)"
source "${ROOT_DIR}"/function.log.sh
source "${ROOT_DIR}/env.sh"

REPO_DIR="${SCRIPT_DIR}/repos"
mkdir -p "${REPO_DIR}"
REPO_UPDATED_FILE="${REPO_DIR}/updated"

TARGET_DIR="${SYSTEM_INSTALLS}"
TARGET_BIN_DIR="${SYSTEM_INSTALLS_BIN}"

AWSLC_TARGET_DIR="${TARGET_DIR}/aws-lc"
AWSLC_VERSION_FILE="${AWSLC_TARGET_DIR}/version"
NGHTTP3_TARGET_DIR="${TARGET_DIR}/nghttp3"
NGHTTP3_VERSION_FILE="${NGHTTP3_TARGET_DIR}/version"
NGTCP2_TARGET_DIR="${TARGET_DIR}/ngtcp2"
NGTCP2_VERSION_FILE="${NGTCP2_TARGET_DIR}/version"
NGHTTP2_TARGET_DIR="${TARGET_DIR}/nghttp2"
NGHTTP2_VERSION_FILE="${NGHTTP2_TARGET_DIR}/version"
CURL_TARGET_DIR="${TARGET_DIR}/curl"
CURL_VERSION_FILE="${CURL_TARGET_DIR}/version"

function ensure_install_script_is_linked () {
  log "INFO" "Linking HTTP tools update script to ${HOME}"
  ln -sf "${SCRIPT_DIR}/install.sh" "${HOME}/.local/bin/update-http-tools"
}

function ensure_aws_lc_is_installed () {
  # https://github.com/aws/aws-lc
  log "INFO" "Install AWS-LC"
  local awslc_repo_dir="${REPO_DIR}/aws-lc"
  if [ ! -d "${awslc_repo_dir}" ]; then
      git clone https://github.com/aws/aws-lc "${awslc_repo_dir}"
  fi
  (
    cd "${awslc_repo_dir}" && git reset --hard HEAD && git checkout main && git pull
    local awslc_version="$(git tag --sort=-version:refname | grep "^v[0-9\.]\+$" | head -n 1)"
    if [ ! -f "${AWSLC_VERSION_FILE}" ] || [[ ! $(cat "${AWSLC_VERSION_FILE}") =~ ^${awslc_version}$ ]] || [ -f "${REPO_UPDATED_FILE}" ]; then
      echo "Installing AWS-LC ${awslc_version}"
      git checkout ${awslc_version}
      git clean -d -f -f -x
      rm -rf "${AWSLC_TARGET_DIR}" || true
      find "${TARGET_BIN_DIR}" -xtype l -delete
      mkdir -p "${AWSLC_TARGET_DIR}"
      # Install with Podman
      podman run --rm -i --userns host -v "${awslc_repo_dir}":"${awslc_repo_dir}":z -v "${AWSLC_TARGET_DIR}":"${AWSLC_TARGET_DIR}":z fedora:41 bash -s <<______HERE
      dnf install -y tar ninja-build cmake gcc gcc-c++ libcurl-devel libarchive-devel zlib-devel xz-devel
      cd "${awslc_repo_dir}"
      # Build dynamic
      cmake -GNinja -B build -DDISABLE_GO=ON -DDISABLE_PERL=ON -DBUILD_TESTING=OFF -DBUILD_SHARED_LIBS=1 -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX="${AWSLC_TARGET_DIR}" -DCMAKE_INSTALL_RPATH="${AWSLC_TARGET_DIR}"/lib64
      ninja -C build
      ninja -C build install
      # Build static
      cmake -GNinja -B build -DDISABLE_GO=ON -DDISABLE_PERL=ON -DBUILD_TESTING=OFF -DBUILD_SHARED_LIBS=0 -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX="${AWSLC_TARGET_DIR}" -DCMAKE_INSTALL_RPATH="${AWSLC_TARGET_DIR}"/lib64
      ninja -C build
      ninja -C build install
______HERE
      
      ln -sr "${AWSLC_TARGET_DIR}/lib64" "${AWSLC_TARGET_DIR}/lib"
      cp --archive --recursive --symbolic-link "${AWSLC_TARGET_DIR}"/bin/* "${TARGET_BIN_DIR}"
      restorecon -RvF "${TARGET_DIR}"

      echo "${awslc_version}" >| "${AWSLC_VERSION_FILE}"
      touch "${REPO_UPDATED_FILE}"
    fi
  )
}

function ensure_nghttp3_is_installed () {
  # https://github.com/ngtcp2/nghttp3
  log "INFO" "Install nghttp3"
  local nghttp3_repo_dir="${REPO_DIR}/nghttp3"
  if [ ! -d "${nghttp3_repo_dir}" ]; then
      git clone https://github.com/ngtcp2/nghttp3 "${nghttp3_repo_dir}"
  fi
  (
    cd "${nghttp3_repo_dir}" && git reset --hard HEAD && git checkout main && git pull
    local nghttp3_version="$(git tag --sort=-version:refname | grep "^v[0-9\.]\+$" | head -n 1)"
    if [ ! -f "${NGHTTP3_VERSION_FILE}" ] || [[ ! $(cat "${NGHTTP3_VERSION_FILE}") =~ ^${nghttp3_version}$ ]] || [ -f "${REPO_UPDATED_FILE}" ]; then
      echo "Installing nghttp3 ${nghttp3_version}"
      git checkout ${nghttp3_version}
      git submodule update --init --recursive
      git clean -d -f -f -x
      git submodule foreach --recursive git clean -d -f -f -x
      git submodule foreach --recursive git reset --hard
      autoreconf -fi
      ./configure --prefix="${NGHTTP3_TARGET_DIR}"
      make -j$(nproc)
      rm -rf "${NGHTTP3_TARGET_DIR}" || true
      make install
      restorecon -RvF "${TARGET_DIR}"
      echo "${nghttp3_version}" >| "${NGHTTP3_VERSION_FILE}"
      touch "${REPO_UPDATED_FILE}"
    fi
  )
}

function ensure_ngtcp2_is_installed () {
  # https://github.com/ngtcp2/ngtcp2
  log "INFO" "Install ngtcp2"
  local ngtcp2_repo_dir="${REPO_DIR}/ngtcp2"
  if [ ! -d "${ngtcp2_repo_dir}" ]; then
      git clone https://github.com/ngtcp2/ngtcp2 "${ngtcp2_repo_dir}"
  fi
  (
    cd "${ngtcp2_repo_dir}" && git reset --hard HEAD && git checkout main && git pull
    local ngtcp2_version="$(git tag --sort=-version:refname | grep "^v[0-9\.]\+$" | head -n 1)"
    if [ ! -f "${NGTCP2_VERSION_FILE}" ] || [[ ! $(cat "${NGTCP2_VERSION_FILE}") =~ ^${ngtcp2_version}$ ]] || [ -f "${REPO_UPDATED_FILE}" ]; then
      echo "Installing ngtcp2 ${ngtcp2_version}"
      git checkout ${ngtcp2_version}
      git submodule update --init --recursive
      git clean -d -f -f -x
      git submodule foreach --recursive git clean -d -f -f -x
      git submodule foreach --recursive git reset --hard
      autoreconf -fi
      ./configure --prefix="${NGTCP2_TARGET_DIR}" --with-boringssl --with-jemalloc --with-libbrotlienc --with-libbrotlidec \
        PKG_CONFIG_PATH="${AWSLC_TARGET_DIR}/lib64/pkgconfig:${NGHTTP3_TARGET_DIR}/lib/pkgconfig:/usr/lib64/pkgconfig" \
        CFLAGS=-fPIE \
        BORINGSSL_CFLAGS="-I${AWSLC_TARGET_DIR}/include" BORINGSSL_LIBS="-L${AWSLC_TARGET_DIR}/lib64 -lssl -lcrypto"
      make -j$(nproc)
      rm -rf "${NGTCP2_TARGET_DIR}" || true
      make install
      restorecon -RvF "${TARGET_DIR}"
      echo "${ngtcp2_version}" >| "${NGTCP2_VERSION_FILE}"
      touch "${REPO_UPDATED_FILE}"
    fi
  )
}

function ensure_nghttp2_is_installed () {
  # https://github.com/nghttp2/nghttp2
  log "INFO" "Install nghttp2"
  local nghttp2_repo_dir="${REPO_DIR}/nghttp2"
  if [ ! -d "${nghttp2_repo_dir}" ]; then
      git clone https://github.com/nghttp2/nghttp2 "${nghttp2_repo_dir}"
  fi
  (
    cd "${nghttp2_repo_dir}" && git reset --hard HEAD && git checkout master && git pull
    local nghttp2_version="$(git tag --sort=-version:refname | grep "^v[0-9\.]\+$" | head -n 1)"
    if [ ! -f "${NGHTTP2_VERSION_FILE}" ] || [[ ! $(cat "${NGHTTP2_VERSION_FILE}") =~ ^${nghttp2_version}$ ]] || [ -f "${REPO_UPDATED_FILE}" ]; then
      echo "Installing nghttp2 ${nghttp2_version}"
      git checkout ${nghttp2_version}
      git submodule update --init --recursive
      git clean -d -f -f -x
      git submodule foreach --recursive git clean -d -f -f -x
      git submodule foreach --recursive git reset --hard
      autoreconf -fi
      ./configure --prefix="${NGHTTP2_TARGET_DIR}" --enable-app --enable-http3  --with-libbpf --with-mruby --with-libbrotlienc --with-libbrotlidec \
        CC=clang CXX=clang++ \
        PKG_CONFIG_PATH="${AWSLC_TARGET_DIR}/lib64/pkgconfig:${NGHTTP3_TARGET_DIR}/lib/pkgconfig:${NGTCP2_TARGET_DIR}/lib/pkgconfig:/usr/lib64/pkgconfig" \
        OPENSSL_CFLAGS="-I${AWSLC_TARGET_DIR}/include" OPENSSL_LIBS="-L${AWSLC_TARGET_DIR}/lib64 -lssl -lcrypto" \
        LDFLAGS="-Wl,-rpath,${AWSLC_TARGET_DIR}/lib64 -Wl,-rpath,${NGHTTP3_TARGET_DIR}/lib -Wl,-rpath,${NGTCP2_TARGET_DIR}/lib"
      make -j$(nproc)
      rm -rf "${NGHTTP2_TARGET_DIR}" || true
      find "${TARGET_BIN_DIR}" -xtype l -delete
      make install
      cp --archive --recursive --symbolic-link "${NGHTTP2_TARGET_DIR}"/bin/* "${TARGET_BIN_DIR}"
      restorecon -RvF "${TARGET_DIR}"
      echo "${nghttp2_version}" >| "${NGHTTP2_VERSION_FILE}"
      touch "${REPO_UPDATED_FILE}"
    fi
  )
}

function ensure_curl_is_installed () {
  # https://github.com/curl/curl
  log "INFO" "Install curl"
  local curl_opts=(-fsSL)
  if [ -n "${GITHUB_API_TOKEN:-}" ]; then
    curl_opts=("${curl_opts[@]}" -H "Authorization: token $GITHUB_API_TOKEN")
  fi
  local curl_version="$(curl "${curl_opts[@]}" https://api.github.com/repos/curl/curl/releases/latest | jq -r .name)"

  if [ ! -f "${CURL_VERSION_FILE}" ] || [[ ! $(cat "${CURL_VERSION_FILE}") =~ ^${curl_version}$ ]] || [ -f "${REPO_UPDATED_FILE}" ]; then
    local curl_repo_dir="${REPO_DIR}/curl"
    rm -rf "${curl_repo_dir}" || true
    mkdir -p "${curl_repo_dir}"
    wget -qO- https://github.com/curl/curl/releases/download/curl-${curl_version//./_}/curl-${curl_version}.tar.gz | tar -xzv --strip-components=1 -C "${curl_repo_dir}"
    cd "${curl_repo_dir}"
    autoreconf -fi
    ./configure --prefix="${CURL_TARGET_DIR}" \
      --with-openssl="${AWSLC_TARGET_DIR}" --with-nghttp3="${NGHTTP3_TARGET_DIR}" --with-nghttp2="${NGHTTP2_TARGET_DIR}" --with-ngtcp2="${NGTCP2_TARGET_DIR}" \
      --enable-websockets --enable-ldap --enable-ldaps --with-libssh2 --with-zlib --with-brotli --with-zstd --with-gssapi --with-libidn2 \
      PKG_CONFIG_PATH="${AWSLC_TARGET_DIR}/lib64/pkgconfig:${NGHTTP3_TARGET_DIR}/lib/pkgconfig:${NGTCP2_TARGET_DIR}/lib/pkgconfig:/usr/lib64/pkgconfig" \
      OPENSSL_CFLAGS="-I${AWSLC_TARGET_DIR}/include" OPENSSL_LIBS="-L${AWSLC_TARGET_DIR}/lib64 -lssl -lcrypto" \
      LDFLAGS="-Wl,-rpath,${AWSLC_TARGET_DIR}/lib64 -Wl,-rpath,${NGHTTP3_TARGET_DIR}/lib -Wl,-rpath,${NGTCP2_TARGET_DIR}/lib"
    make -j$(nproc)
    rm -rf "${CURL_TARGET_DIR}" || true
    find "${TARGET_BIN_DIR}" -xtype l -delete
    make install
    cp --archive --recursive --symbolic-link "${CURL_TARGET_DIR}"/bin/* "${TARGET_BIN_DIR}"
    restorecon -RvF "${TARGET_DIR}"
    echo "${curl_version}" >| "${CURL_VERSION_FILE}"
    touch "${REPO_UPDATED_FILE}"
  fi
}

rm -f "${REPO_UPDATED_FILE}" || true
ensure_install_script_is_linked

ensure_aws_lc_is_installed
ensure_nghttp3_is_installed
ensure_ngtcp2_is_installed
ensure_nghttp2_is_installed
ensure_curl_is_installed
