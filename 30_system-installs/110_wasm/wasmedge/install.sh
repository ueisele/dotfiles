#!/usr/bin/env bash
set -e
SCRIPT_DIR="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")"
ROOT_DIR="$(readlink -f "${SCRIPT_DIR}/../../..")"
source "${ROOT_DIR}/function.log.sh"
source "${ROOT_DIR}/env.sh"

INSTALLS_DIR="${SYSTEM_INSTALLS}/wasmedge"
VERSION_FILE="${INSTALLS_DIR}/version"

function ensure_wasmedge_is_installed_from_source () {
  # https://github.com/WasmEdge/WasmEdge
  # requires: cmake gcc gcc-c++ clang lld-devel llvm-devel zlib-devel libffi-devel
  log "INFO" "Install wasmedge"
  local build_dir="${SCRIPT_DIR}/build"
  if [ ! -d "${build_dir}" ]; then
      git clone https://github.com/WasmEdge/WasmEdge "${build_dir}"
  fi
  (
    cd "${build_dir}" && git reset --hard HEAD && git checkout master && git pull
    local version="$(git tag --sort=-version:refname | grep "^[0-9\.]\+(-rc.*)?$" | head -n 1)"
    if [ ! -f "${VERSION_FILE}" ] || [[ ! $(cat "${VERSION_FILE}") =~ ^${version}$ ]]; then
      log "INFO" "Install wasmedge ${version} to ${INSTALLS_DIR}"
      git checkout ${version}
      git clean -d -f -f -x
      # build
      mkdir -p build && cd build
      cmake -DCMAKE_INSTALL_PREFIX="${INSTALLS_DIR:?}" -DCMAKE_BUILD_TYPE=Release ../
      make -j$(nproc)
      # install
      rm -rf "${INSTALLS_DIR:?}" || true
      find "${SYSTEM_INSTALLS_BIN}/" -xtype l -delete
      make install
      chmod -R ug=rwX,o=rX "${INSTALLS_DIR}"
      cp --archive --recursive --symbolic-link "${INSTALLS_DIR}"/bin/{wasmedge,wasmedgec} "${SYSTEM_INSTALLS_BIN}"
      restorecon -RvF "${SYSTEM_INSTALLS}"
      echo "${version}" >| "${VERSION_FILE}"
    fi
  )
}

if [ -d "${SYSTEM_INSTALLS}" ]; then
  ensure_wasmedge_is_installed_from_source
fi
