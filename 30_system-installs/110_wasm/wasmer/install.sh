#!/usr/bin/env bash
set -e
SCRIPT_DIR="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")"
ROOT_DIR="$(readlink -f "${SCRIPT_DIR}/../../..")"
source "${ROOT_DIR}/function.log.sh"
source "${ROOT_DIR}/env.sh"

INSTALLS_DIR="${SYSTEM_INSTALLS}/wasmer"
VERSION_FILE="${INSTALLS_DIR}/version"

function ensure_wasmer_is_installed_from_source () {
  # https://github.com/wasmerio/wasmer
  # requires for llvm: gcc gcc-c++ clang llvm15-devel libffi-devel zlib-devel
  log "INFO" "Install wasmer with rust"
  local build_dir="${SCRIPT_DIR}/build"
  if [ ! -d "${build_dir}" ]; then
      git clone https://github.com/wasmerio/wasmer.git "${build_dir}"
  fi
  (
    cd "${build_dir}" && git reset --hard HEAD && git checkout main && git remote prune origin && git pull
    local version="$(git tag --sort=-version:refname | grep "^v[0-9\.]\+$" | head -n 1)"
    if [ ! -f "${VERSION_FILE}" ] || [[ ! $(cat "${VERSION_FILE}") =~ ^${version}$ ]]; then
      log "INFO" "Install wasmer ${version} to ${INSTALLS_DIR}"
      git checkout ${version}
      git clean -d -f -f -x
      # build
      cargo clean
      ## ToDo: enable llvm
      ENABLE_LLVM=0 make build-wasmer
      make build-capi
      make package-capi
      # install
      rm -rf "${INSTALLS_DIR:?}" || true
      find "${SYSTEM_INSTALLS_BIN}/" -xtype l -delete
      mkdir -p "${INSTALLS_DIR}"/{bin,lib,include}
      cp ./target/release/wasmer "${INSTALLS_DIR}"/bin/
      cp ./package/lib/* "${INSTALLS_DIR}"/lib/
      cp ./package/include/* "${INSTALLS_DIR}"/include/
      chmod -R ug=rwX,o=rX "${INSTALLS_DIR}"
      cp --archive --recursive --symbolic-link "${INSTALLS_DIR}"/bin/wasmer "${SYSTEM_INSTALLS_BIN}"
      restorecon -RvF "${SYSTEM_INSTALLS}"
      echo "${version}" >| "${VERSION_FILE}"
      # cleanup
      git clean -d -f -f -x
    fi
  )
}

if [ -d "${SYSTEM_INSTALLS}" ]; then
  ensure_wasmer_is_installed_from_source
fi
