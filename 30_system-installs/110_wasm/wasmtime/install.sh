#!/usr/bin/env bash
set -e
SCRIPT_DIR="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")"
ROOT_DIR="$(readlink -f "${SCRIPT_DIR}/../../..")"
source "${ROOT_DIR}/function.log.sh"
source "${ROOT_DIR}/env.sh"

INSTALLS_DIR="${SYSTEM_INSTALLS}/wasmtime"
VERSION_FILE="${INSTALLS_DIR}/version"

function ensure_wasmtime_is_installed_from_source () {
  # https://docs.wasmtime.dev/cli-install.html
  log "INFO" "Install wasmtime with rust"
  local build_dir="${SCRIPT_DIR}/build"
  if [ ! -d "${build_dir}" ]; then
      git clone https://github.com/bytecodealliance/wasmtime.git "${build_dir}"
  fi
  (
    cd "${build_dir}" && git reset --hard HEAD && git checkout main && git pull
    local version="$(git tag --sort=-version:refname | grep "^v[0-9\.]\+$" | head -n 1)"
    if [ ! -f "${VERSION_FILE}" ] || [[ ! $(cat "${VERSION_FILE}") =~ ^${version}$ ]]; then
      log "INFO" "Install wasmtime ${version} to ${INSTALLS_DIR}"
      git checkout ${version}
      git submodule update --init --recursive
      git clean -d -f -f -x
      git submodule foreach --recursive git clean -d -f -f -x
      git submodule foreach --recursive git reset --hard
      # build
      cargo clean
      export CARGO_PROFILE_RELEASE_STRIP=debuginfo
      export CARGO_PROFILE_RELEASE_PANIC=abort
      cargo build --release --package=wasmtime-cli --features=winch --locked
      mkdir -p target/c-api-build
      cd target/c-api-build
      cmake \
        ../../crates/c-api \
        -DCMAKE_BUILD_TYPE=Release \
        -DCMAKE_INSTALL_PREFIX=../c-api-install \
        -DCMAKE_INSTALL_LIBDIR=../c-api-install/lib
      cmake --build . --target install
      cd ../../
      # install
      rm -rf "${INSTALLS_DIR:?}" || true
      find "${SYSTEM_INSTALLS_BIN}/" -xtype l -delete
      mkdir -p "${INSTALLS_DIR}"/bin
      cp ./target/release/wasmtime "${INSTALLS_DIR}"/bin/
      cp -r ./target/c-api-install/* "${INSTALLS_DIR}"
      chmod -R ug=rwX,o=rX "${INSTALLS_DIR}"
      cp --archive --recursive --symbolic-link "${INSTALLS_DIR}"/bin/wasmtime "${SYSTEM_INSTALLS_BIN}"
      restorecon -RvF "${SYSTEM_INSTALLS}"
      echo "${version}" >| "${VERSION_FILE}"
    fi
  )
}

if [ -d "${SYSTEM_INSTALLS}" ]; then
  ensure_wasmtime_is_installed_from_source
fi
