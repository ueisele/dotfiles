#!/usr/bin/env bash
set -e
SCRIPT_DIR="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")"
ROOT_DIR="$(readlink -f "${SCRIPT_DIR}/../../..")"
source "${ROOT_DIR}/function.log.sh"
source "${ROOT_DIR}/env.sh"

INSTALLS_DIR="${SYSTEM_INSTALLS}/spin"
VERSION_FILE="${INSTALLS_DIR}/version"

function ensure_spin_is_installed_from_source () {
  # Fermyon Spin (https://developer.fermyon.com/spin/install#using-cargo-to-install-spin)
  # Requires: openssl-devel
  log "INFO" "Install Fermyon Spin with Rust"
  local build_dir="${SCRIPT_DIR}/build"
  if [ ! -d "${build_dir}" ]; then
      git clone https://github.com/fermyon/spin "${build_dir}"
  fi
  (
    cd "${build_dir}" && git reset --hard HEAD && git checkout main && git pull
    local version="$(git tag --sort=-version:refname | grep "^v[0-9\.]\+$" | head -n 1)"
    if [ ! -f "${VERSION_FILE}" ] || [[ ! $(cat "${VERSION_FILE}") =~ ^${version}$ ]]; then
      log "INFO" "Install spin ${version} to ${INSTALLS_DIR}"
      git checkout ${version}
      git clean -d -f -f -x
      # clean
      rm -rf "${INSTALLS_DIR:?}" || true
      find "${SYSTEM_INSTALLS_BIN}/" -xtype l -delete
      mkdir -p "${INSTALLS_DIR}"/bin
      # build (dynamically linked)
      cargo clean
      cargo update -p time # https://github.com/rust-lang/rust/issues/125319
      cargo build --release --package=spin-cli --locked
      # install (dynamically linked)
      cp ./target/release/spin "${INSTALLS_DIR}"/bin/spin
      # build (statically linked)
      # => required by crun to support spin
      cargo clean
      RUSTFLAGS="-C target-feature=+crt-static -C link-self-contained=yes" \
      cross build --target x86_64-unknown-linux-musl --release --package=spin-cli --features openssl/vendored --locked
      # install (statically linked)
      cp ./target/x86_64-unknown-linux-musl/release/spin "${INSTALLS_DIR}"/bin/spin-static
      # finalize
      chmod -R ug=rwX,o=rX "${INSTALLS_DIR}"
      cp --archive --recursive --symbolic-link "${INSTALLS_DIR}"/bin/* "${SYSTEM_INSTALLS_BIN}"
      restorecon -RvF "${SYSTEM_INSTALLS}"
      echo "${version}" >| "${VERSION_FILE}"
    fi
  )
}

function ensure_spin_templates_and_plugins_are_installed () {
  # target dir: ~/.local/share/spin
  spin templates install --git https://github.com/fermyon/spin --upgrade
  spin templates install --git https://github.com/fermyon/spin-python-sdk --upgrade
  spin templates install --git https://github.com/fermyon/spin-js-sdk --upgrade
  spin templates install --git https://github.com/fermyon/spin-dotnet-sdk --upgrade
  spin templates install --git https://github.com/fermyon/leptos-spin.git --upgrade # also see https://github.com/leptos-rs/start-spin
  # message triger template and plugin: https://github.com/lee-orr/spin-message-trigger
  spin plugins update
  spin plugins install js2wasm --yes
  spin plugins install py2wasm --yes
}

if [ -d "${SYSTEM_INSTALLS}" ]; then
  ensure_spin_is_installed_from_source
  ensure_spin_templates_and_plugins_are_installed
fi
