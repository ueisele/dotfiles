#!/usr/bin/env bash
set -e
SCRIPT_DIR="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")"
ROOT_DIR="$(readlink -f "${SCRIPT_DIR}/../..")"
source "${ROOT_DIR}/function.log.sh"
source "${ROOT_DIR}/env.sh"

function ensure_static_spin_binary_is_copied_to_usr_local_bin () {
  # at the moment crun expects that static spin is at /usr/local/bin/spin
  # see https://github.com/containers/crun/blob/main/src/libcrun/handlers/spin.c
  local local_bin_dir="/usr/local/bin"
  log "INFO" "Ensure ${local_bin_dir} exists"
  if [ ! -d "${local_bin_dir}" ]; then
    mkdir -p ${local_bin_dir}
    chmod -R u=rwX,go=rX ${local_bin_dir}
    chown -R root:root ${local_bin_dir}
    restorecon -RvF ${local_bin_dir}
  fi
  if [ -e "${SYSTEM_INSTALLS}/spin/bin/spin-static" ]; then
    log "INFO" "Copy spin to ${local_bin_dir}"
    \cp --link "${SYSTEM_INSTALLS}"/spin/bin/spin-static /usr/local/bin/spin
  fi
}

if [ -z "${SUDO_USER}" ]; then
  echo "The script must be executed with sudo!"
elif [ -d "${SYSTEM_INSTALLS}" ]; then
  ensure_static_spin_binary_is_copied_to_usr_local_bin
fi
