#!/usr/bin/env bash
set -e
SCRIPT_DIR="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")"
ROOT_DIR="$(readlink -f "${SCRIPT_DIR}/../../..")"
source "${ROOT_DIR}/function.log.sh"
source "${ROOT_DIR}/env.sh"

INSTALLS_DIR="${SYSTEM_INSTALLS}/cni"
VERSION_FILE="${INSTALLS_DIR}/version"

function ensure_cni_is_installed () {
  # https://github.com/containernetworking/plugins/releases
  local curl_opts=(-fsSL)
  if [ -n "${GITHUB_API_TOKEN:-}" ]; then
    curl_opts=("${curl_opts[@]}" -H "Authorization: token $GITHUB_API_TOKEN")
  fi
  local version="$(curl "${curl_opts[@]}" https://api.github.com/repos/containernetworking/plugins/releases | jq -r '.[] | select(.author.login=="squeed") | .tag_name' | sed '/-/!{s/$/_/}' | sort --version-sort -r | sed 's/_$//' | head -n1)"
  local url="https://github.com/containernetworking/plugins/releases/download/${version}/cni-plugins-linux-amd64-${version}.tgz"

  if [ ! -f "${VERSION_FILE}" ] || [[ ! $(cat "${VERSION_FILE}") =~ ^${version}$ ]]; then
    log "INFO" "Install CNI ${version} to ${INSTALLS_DIR}"
    rm -rf "${INSTALLS_DIR:?}"/bin/* || true
    mkdir -p "${INSTALLS_DIR}/bin"
    curl "${curl_opts[@]}" "${url}" | tar -xzv -C "${INSTALLS_DIR}/bin"
    chmod -R ug=rwX,o=rX "${INSTALLS_DIR}"
    restorecon -RvF "${SYSTEM_INSTALLS}"
    echo "${version}" >| "${VERSION_FILE}"
  fi
}

if [ -d "${SYSTEM_INSTALLS}" ]; then
  ensure_cni_is_installed
fi
