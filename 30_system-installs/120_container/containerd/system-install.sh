#!/usr/bin/env bash
set -e
SCRIPT_DIR="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")"
ROOT_DIR="$(readlink -f "${SCRIPT_DIR}/../../..")"
source "${ROOT_DIR}/function.log.sh"
source "${ROOT_DIR}/env.sh"

function ensure_data_dir_exists () {
  local data_dir="/var/lib/containerd"
  log "INFO" "Ensure data dir ${data_dir} exists"
  if [ ! -d "${data_dir}" ]; then
    mkdir -p ${data_dir}
    chmod -R u=rwX,go= ${data_dir}
    chown -R root:root ${data_dir}
    restorecon -RvF ${data_dir}
  fi
}

function ensure_selinux_label_set () {
  log "INFO" "Set SELinux labels for containerd"
  local containerd_pattern="${SYSTEM_INSTALLS}(/containerd)?/bin/containerd.*"
  (semanage fcontext -l | grep "${containerd_pattern}") || semanage fcontext -a -t container_runtime_exec_t "${containerd_pattern}"
  local ctr_pattern="${SYSTEM_INSTALLS}(/containerd)?/bin/ctr"
  (semanage fcontext -l | grep "${ctr_pattern}") || semanage fcontext -a -t container_runtime_exec_t "${ctr_pattern}"
  restorecon -RvF "${SYSTEM_INSTALLS}/containerd/bin"
  restorecon -RvF "${SYSTEM_INSTALLS}/bin"
}

function ensure_containerd_config_is_installed () {
  log "INFO" "Install containerd config"
  mkdir -p /etc/containerd/{certs.d,cri/cni/net.d}
  \cp -f "${SCRIPT_DIR}/system-files/etc/containerd/config.toml" /etc/containerd/config.toml
  \cp -f "${SCRIPT_DIR}/system-files/etc/containerd/cri/cni/net.d/cri-bridge.conflist" /etc/containerd/cri/cni/net.d/
  chmod -R u=rwX,go=rX /etc/containerd
  chown -R root:root /etc/containerd
}

function ensure_kernel_modules_enabled () {
  log "INFO" "Enable kernel modules required by containerd"
  \cp -f "${SCRIPT_DIR}/system-files/etc/modules-load.d/50-containerd.conf" /etc/modules-load.d/50-containerd.conf
  chmod u=rwX,go=rX /etc/modules-load.d/50-containerd.conf
  chown root:root /etc/modules-load.d/50-containerd.conf
  \cp -f "${SCRIPT_DIR}/system-files/etc/sysctl.d/50-containerd.conf" /etc/sysctl.d/50-containerd.conf
  chmod u=rwX,go=rX /etc/sysctl.d/50-containerd.conf
  chown root:root /etc/sysctl.d/50-containerd.conf
}

function ensure_systemd_service_is_enabled () {
  log "INFO" "Enable containerd systemd service"
  \cp -f "${SCRIPT_DIR}"/system-files/etc/systemd/system/containerd.service /etc/systemd/system/
  chmod u=rwX,go=rX /etc/systemd/system/containerd.service
  chown root:root /etc/systemd/system/containerd.service
  systemctl daemon-reload
  systemctl enable containerd.service
  systemctl start containerd.service
}

if [ -z "${SUDO_USER}" ]; then
  echo "The script must be executed with sudo!"
elif [ -d "${SYSTEM_INSTALLS}" ]; then
  ensure_data_dir_exists
  ensure_selinux_label_set
  ensure_containerd_config_is_installed
  ensure_kernel_modules_enabled
  ensure_systemd_service_is_enabled
fi
