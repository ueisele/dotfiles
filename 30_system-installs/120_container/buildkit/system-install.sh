#!/usr/bin/env bash
set -e
SCRIPT_DIR="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")"
ROOT_DIR="$(readlink -f "${SCRIPT_DIR}/../../..")"
source "${ROOT_DIR}/function.log.sh"
source "${ROOT_DIR}/env.sh"

function ensure_data_dir_exists () {
  local data_dir="/var/lib/buildkit"
  log "INFO" "Ensure data dir ${data_dir} exists"
  if [ ! -d "${data_dir}" ]; then
    mkdir -p ${data_dir}
    chmod -R u=rwX,go= ${data_dir}
    chown -R root:root ${data_dir}
    restorecon -RvF ${data_dir}
  fi
}

function ensure_selinux_label_set () {
  log "INFO" "Set SELinux labels for buildkit"
  local buildkitd_pattern="${SYSTEM_INSTALLS}(/buildkit)?/bin/buildkitd"
  (semanage fcontext -l | grep "${buildkitd_pattern}") || semanage fcontext -a -t container_runtime_exec_t "${buildkitd_pattern}"
  local buildctl_pattern="${SYSTEM_INSTALLS}(/buildkit)?/bin/buildctl"
  (semanage fcontext -l | grep "${buildctl_pattern}") || semanage fcontext -a -t container_runtime_exec_t "${buildctl_pattern}"
  restorecon -RvF "${SYSTEM_INSTALLS}/buildkit/bin"
  restorecon -RvF "${SYSTEM_INSTALLS}/bin"
}

function ensure_buildkit_config_is_installed () {
  log "INFO" "Install buildkit config"
  mkdir -p /etc/buildkit
  \cp -f "${SCRIPT_DIR}/system-files/etc/buildkit/buildkitd.toml" /etc/buildkit/
  \cp -f "${SCRIPT_DIR}/system-files/etc/buildkit/cni.conflist" /etc/buildkit/
  chmod -R u=rwX,go=rX /etc/buildkit
  chown -R root:root /etc/buildkit
}

function ensure_systemd_service_is_enabled () {
  log "INFO" "Enable buildkit systemd service"
  \cp -f "${SCRIPT_DIR}"/system-files/etc/systemd/system/buildkit.{service,socket} /etc/systemd/system/
  chmod u=rwX,go=rX /etc/systemd/system/buildkit.{service,socket}
  chown root:root /etc/systemd/system/buildkit.{service,socket}
  systemctl daemon-reload
  systemctl enable buildkit.socket
  systemctl start buildkit.socket
}

if [ -z "${SUDO_USER}" ]; then
  echo "The script must be executed with sudo!"
elif [ -d "${SYSTEM_INSTALLS}" ]; then
  ensure_data_dir_exists
  ensure_selinux_label_set
  ensure_buildkit_config_is_installed
  ensure_systemd_service_is_enabled
fi
