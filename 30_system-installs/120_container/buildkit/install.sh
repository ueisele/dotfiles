#!/usr/bin/env bash
set -e
SCRIPT_DIR="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")"
ROOT_DIR="$(readlink -f "${SCRIPT_DIR}/../../..")"
source "${ROOT_DIR}/function.log.sh"
source "${ROOT_DIR}/env.sh"

INSTALLS_DIR="${SYSTEM_INSTALLS}/buildkit"
VERSION_FILE="${INSTALLS_DIR}/version"

function ensure_buildkit_is_installed_from_source () {
  # https://github.com/moby/buildkit
  log "INFO" "Install buildkit with go"
  local build_dir="${SCRIPT_DIR}/build"
  if [ ! -d "${build_dir}" ]; then
      git clone https://github.com/moby/buildkit.git "${build_dir}"
  fi
  (
    cd "${build_dir}" && git reset --hard HEAD && git checkout master && git pull
    local version="$(git tag --sort=-version:refname | grep "^v[0-9\.]\+$" | head -n 1)"
    if [ ! -f "${VERSION_FILE}" ] || [[ ! $(cat "${VERSION_FILE}") =~ ^${version}$ ]]; then
      log "INFO" "Install BuildKit ${version} to ${INSTALLS_DIR}"
      [ -d "./bin" ] && rm -rf ./bin
      git checkout ${version}
      git clean -d -f -f -x
      local pkg="github.com/moby/buildkit"
      local revision="$(git rev-parse HEAD)"
      local ldflags="-X ${pkg}/version.Version=${version} -X ${pkg}/version.Revision=${revision} -X ${pkg}/version.Package=${pkg}"
      # build buildkitd
      CGO_ENABLED=1 go build -ldflags "${ldflags} -s -w -extldflags '-static'" -tags "netgo osusergo static_build seccomp selinux" -o bin/buildkitd ./cmd/buildkitd
      # build buildctl
      CGO_ENABLED=1 go build -ldflags "${ldflags} -s -w -extldflags '-static'" -tags "netgo osusergo static_build" -o bin/buildctl ./cmd/buildctl
      # install
      rm -f "${INSTALLS_DIR:?}"/bin/* || true
      find "${SYSTEM_INSTALLS_BIN}/" -xtype l -delete
      mkdir -p "${INSTALLS_DIR}"/bin
      cp bin/* "${INSTALLS_DIR}"/bin
      chmod -R ug=rwX,o=rX "${INSTALLS_DIR}/bin"
      cp --archive --recursive --symbolic-link "${INSTALLS_DIR}"/bin/{buildctl,buildkitd} "${SYSTEM_INSTALLS_BIN}"
      restorecon -RvF "${SYSTEM_INSTALLS}"
      echo "${version}" >| "${VERSION_FILE}"
    fi
  )
}

if [ -d "${SYSTEM_INSTALLS}" ]; then
  ensure_buildkit_is_installed_from_source
fi
