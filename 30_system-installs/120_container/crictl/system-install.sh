#!/usr/bin/env bash
set -e
SCRIPT_DIR="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")"
ROOT_DIR="$(readlink -f "${SCRIPT_DIR}/../../..")"
source "${ROOT_DIR}/function.log.sh"
source "${ROOT_DIR}/env.sh"

function ensure_selinux_label_set () {
  log "INFO" "Set SELinux labels for crictl"
  local crictld_pattern="${SYSTEM_INSTALLS}(/crictl)?/bin/crictl"
  (semanage fcontext -l | grep "${crictld_pattern}") || semanage fcontext -a -t container_runtime_exec_t "${crictld_pattern}"
  restorecon -RvF "${SYSTEM_INSTALLS}/crictl/bin"
  restorecon -RvF "${SYSTEM_INSTALLS}/bin"
}

function ensure_crictl_config_is_installed () {
  log "INFO" "Install crictl config"
  \cp -f "${SCRIPT_DIR}/system-files/etc/crictl.yaml" /etc/
  chmod -R u=rwX,go=rX /etc/crictl.yaml
  chown -R root:root /etc/crictl.yaml
}

if [ -z "${SUDO_USER}" ]; then
  echo "The script must be executed with sudo!"
elif [ -d "${SYSTEM_INSTALLS}" ]; then
  ensure_selinux_label_set
  ensure_crictl_config_is_installed
fi
