#!/usr/bin/env bash
set -e
SCRIPT_DIR="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")"
ROOT_DIR="$(readlink -f "${SCRIPT_DIR}/../../..")"
source "${ROOT_DIR}/function.log.sh"
source "${ROOT_DIR}/env.sh"

INSTALLS_DIR="${SYSTEM_INSTALLS}/crictl"
VERSION_FILE="${INSTALLS_DIR}/version"

function ensure_crictl_is_installed_from_source () {
  # https://github.com/kubernetes-sigs/cri-tools
  log "INFO" "Install crictl with go"
  local build_dir="${SCRIPT_DIR}/build"
  if [ ! -d "${build_dir}" ]; then
      git clone https://github.com/kubernetes-sigs/cri-tools.git "${build_dir}"
  fi
  (
    cd "${build_dir}" && git reset --hard HEAD && git checkout master && git pull
    local version="$(git describe --tags --dirty --always)"
    if [ ! -f "${VERSION_FILE}" ] || [[ ! $(cat "${VERSION_FILE}") =~ ^${version}$ ]]; then
      log "INFO" "Install crictl ${version} to ${INSTALLS_DIR}"
      [ -d "./bin" ] && rm -rf ./bin
      git checkout ${version}
      git clean -d -f -f -x
      local pkg="github.com/kubernetes-sigs/cri-tools"
      local ldflags="-X ${pkg}/pkg/version.Version=${version}"
      # build crictl
      CGO_ENABLED=1 go build -ldflags "${ldflags} -s -w -extldflags '-static'" -tags "netgo osusergo static_build" -trimpath -o bin/crictl ./cmd/crictl
      # install
      rm -f "${INSTALLS_DIR:?}"/bin/* || true
      find "${SYSTEM_INSTALLS_BIN}/" -xtype l -delete
      mkdir -p "${INSTALLS_DIR}"/bin
      cp bin/* "${INSTALLS_DIR}"/bin
      chmod -R ug=rwX,o=rX "${INSTALLS_DIR}/bin"
      cp --archive --recursive --symbolic-link "${INSTALLS_DIR}"/bin/crictl "${SYSTEM_INSTALLS_BIN}"
      restorecon -RvF "${SYSTEM_INSTALLS}"
      echo "${version}" >| "${VERSION_FILE}"
    fi
  )
}

function ensure_shell_completion_is_created () {
  log "INFO" "Shell completion for crictl"
  "${INSTALLS_DIR}"/bin/crictl completion zsh >| "${HOME}/.zsh/zfunc.d/_crictl"
}

if [ -d "${SYSTEM_INSTALLS}" ]; then
  ensure_crictl_is_installed_from_source
  ensure_shell_completion_is_created
fi
