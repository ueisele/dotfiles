#!/usr/bin/env bash
set -e
SCRIPT_DIR="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")"
ROOT_DIR="$(readlink -f "${SCRIPT_DIR}/../../..")"
source "${ROOT_DIR}/function.log.sh"
source "${ROOT_DIR}/env.sh"

INSTALLS_DIR="${SYSTEM_INSTALLS}/nerdctl"
VERSION_FILE="${INSTALLS_DIR}/version"

function ensure_nerdctl_is_installed () {
  # https://github.com/containerd/nerdctl/releases
  local curl_opts=(-fsSL)
  if [ -n "${GITHUB_API_TOKEN:-}" ]; then
    curl_opts=("${curl_opts[@]}" -H "Authorization: token $GITHUB_API_TOKEN")
  fi
  local version="$(curl "${curl_opts[@]}" https://api.github.com/repos/containerd/nerdctl/releases | jq -r '.[].tag_name' | sed '/-/!{s/$/_/}' | sort --version-sort -r | sed 's/_$//' | head -n1)"
  local url="https://github.com/containerd/nerdctl/releases/download/${version}/nerdctl-${version#v}-linux-amd64.tar.gz"

  if [ ! -f "${VERSION_FILE}" ] || [[ ! $(cat "${VERSION_FILE}") =~ ^${version}$ ]]; then
    log "INFO" "Install nerdctl ${version} to ${INSTALLS_DIR}"
    rm -f "${INSTALLS_DIR:?}"/bin/* || true
    find "${SYSTEM_INSTALLS_BIN}/" -xtype l -delete
    mkdir -p "${INSTALLS_DIR}"/bin
    curl "${curl_opts[@]}" "${url}" | tar -xzv -C "${INSTALLS_DIR}/bin"
    chmod -R ug=rwX,o=rX "${INSTALLS_DIR}/bin"
    cp --archive --recursive --symbolic-link "${INSTALLS_DIR}"/bin/nerdctl "${SYSTEM_INSTALLS_BIN}"
    restorecon -RvF "${SYSTEM_INSTALLS}"
    echo "${version}" >| "${VERSION_FILE}"
  fi
}

function ensure_shell_completion_is_created () {
  log "INFO" "Shell completion for nerdctl"
  "${INSTALLS_DIR}"/bin/nerdctl completion zsh >| "${HOME}/.zsh/zfunc.d/_nerdctl"
}

if [ -d "${SYSTEM_INSTALLS}" ]; then
  ensure_nerdctl_is_installed
  ensure_shell_completion_is_created
fi
