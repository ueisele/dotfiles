#!/usr/bin/env bash
set -e
SCRIPT_DIR="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")"
ROOT_DIR="$(readlink -f "${SCRIPT_DIR}/../../..")"
source "${ROOT_DIR}/function.log.sh"
source "${ROOT_DIR}/env.sh"

function ensure_data_dir_exists () {
  local data_dir="/var/lib/nerdctl"
  log "INFO" "Ensure data dir ${data_dir} exists"
  if [ ! -d "${data_dir}" ]; then
    mkdir -p ${data_dir}
    chmod -R u=rwX,go= ${data_dir}
    chown -R root:root ${data_dir}
    restorecon -RvF ${data_dir}
  fi
}

function ensure_selinux_label_set () {
  log "INFO" "Set SELinux labels for nerdctl"
  local nerdctl_pattern="${SYSTEM_INSTALLS}(/nerdctl)?/bin/nerdctl"
  (semanage fcontext -l | grep "${nerdctl_pattern}") || semanage fcontext -a -t container_runtime_exec_t "${nerdctl_pattern}"
  restorecon -RvF "${SYSTEM_INSTALLS}/nerdctl/bin/nerdctl"
  restorecon -RvF "${SYSTEM_INSTALLS}/bin/nerdctl"
}

function ensure_nerdctl_config_is_installed () {
  log "INFO" "Install nerdctl config"
  mkdir -p /etc/nerdctl/cni/net.d
  \cp -f "${SCRIPT_DIR}/system-files/etc/nerdctl/nerdctl.toml" /etc/nerdctl/nerdctl.toml
  \cp -f "${SCRIPT_DIR}/system-files/etc/nerdctl/cni/net.d/nerdctl-bridge.conflist" /etc/nerdctl/cni/net.d/
  chmod -R u=rwX,go=rX /etc/nerdctl
  chown -R root:root /etc/nerdctl
}

if [ -z "${SUDO_USER}" ]; then
  echo "The script must be executed with sudo!"
elif [ -d "${SYSTEM_INSTALLS}" ]; then
  ensure_data_dir_exists
  ensure_selinux_label_set
  ensure_nerdctl_config_is_installed
fi
