#!/usr/bin/env bash
set -e
SCRIPT_DIR="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")"
ROOT_DIR="$(readlink -f "${SCRIPT_DIR}/../../..")"
source "${ROOT_DIR}/function.log.sh"
source "${ROOT_DIR}/env.sh"

function ensure_selinux_label_set () {
  log "INFO" "Set SELinux labels for crun"
  local crun_pattern="${SYSTEM_INSTALLS}(/crun)?/bin/crun"
  (semanage fcontext -l | grep "${crun_pattern}") || semanage fcontext -a -t container_runtime_exec_t "${crun_pattern}"
  restorecon -RvF "${SYSTEM_INSTALLS}/crun/bin/crun"
  restorecon -RvF "${SYSTEM_INSTALLS}/bin/crun"
}

if [ -z "${SUDO_USER}" ]; then
  echo "The script must be executed with sudo!"
elif [ -d "${SYSTEM_INSTALLS}" ]; then
  ensure_selinux_label_set
fi
