= crun

== Examples

=== WASM

==== Podman

.Simple example which demonstrates the WASI interface:
[source,bash]
----
podman run --rm --annotation run.oci.handler=wasm wasmedge/example-wasi:latest /wasi_example_main.wasm 50000000
----

.Startup time can be further reduced by disabling cgroup support and using host network:
[source,bash]
----
podman run --rm --annotation run.oci.handler=wasmtime --systemd=false --cgroups=disabled --net host wasmedge/example-wasi:latest /wasi_example_main.wasm 50000000
----

.Example which makes use of the wasmedge http extension:
[source,bash]
----
podman run -d --name wasmedge-http --annotation run.oci.handler=wasmedge --net host wasmedge/example-wasi-http:latest
echo hello world | curl -d@- http://localhost:1234
podman rm -f wasmedge-http
----

.The following image has been published for OS/Arch `wasi/wasm`:
[source,bash]
----
podman run --rm --annotation run.oci.handler=wasm --platform=wasi/wasm secondstate/rust-example-hello:latest
----

.Issue: Podman has a very large startup time if a platform is specified. Without it, it is much faster.
[source,bash]
----
podman run --rm --annotation run.oci.handler=wasm secondstate/rust-example-hello:latest
----

==== Docker

.Simple example which demonstrates the WASI interface:
[source,bash]
----
docker run --rm --runtime=crun --annotation run.oci.handler=wasm wasmedge/example-wasi:latest /wasi_example_main.wasm 50000000
----

.Startup time can be further reduced by using host network:
[source,bash]
----
docker run --rm --runtime=crun --annotation run.oci.handler=wasm --net host wasmedge/example-wasi:latest /wasi_example_main.wasm 50000000
----

.Example which makes use of the wasmedge http extension:
[source,bash]
----
docker run -d --name wasmedge-http --runtime=crun --annotation run.oci.handler=wasmedge --net host wasmedge/example-wasi-http:latest
echo hello world | curl -d@- http://localhost:1234
docker rm -f wasmedge-http
----

.The following image has been published for OS/Arch `wasi/wasm`:
[source,bash]
----
docker run --rm --runtime=crun --annotation run.oci.handler=wasm --platform=wasi/wasm secondstate/rust-example-hello:latest
----

=== libkrun

https://github.com/containers/libkrun

==== Podman

[source,bash]
----
podman run --rm -t --annotation run.oci.handler=krun docker.io/fedora:40 uname -a
----

==== Docker

[source,bash]
----
docker run --runtime=crun --rm -t --annotation run.oci.handler=krun docker.io/fedora:40 uname -a
----
