#!/usr/bin/env bash
set -e
SCRIPT_DIR="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")"
ROOT_DIR="$(readlink -f "${SCRIPT_DIR}/../../..")"
source "${ROOT_DIR}/function.log.sh"
source "${ROOT_DIR}/env.sh"

INSTALLS_DIR="${SYSTEM_INSTALLS}/crun"
VERSION_FILE="${INSTALLS_DIR}/version"

function ensure_crun_is_installed_from_source () {
  # https://github.com/containers/crun
  # https://github.com/containers/crun/blob/main/tests/wasmedge-build/Dockerfile
  # requires: yajl-devel criu-devel
  log "INFO" "Install crun"
  local build_dir="${SCRIPT_DIR}/build"
  if [ ! -d "${build_dir}" ]; then
      git clone https://github.com/containers/crun.git "${build_dir}"
  fi
  (
    cd "${build_dir}" && git reset --hard HEAD && git checkout main && git pull
    local version="$(git tag --sort=-version:refname | grep "^[0-9\.]\+$" | head -n 1)"
    if [ ! -f "${VERSION_FILE}" ] || [[ ! $(cat "${VERSION_FILE}") =~ ^${version}$ ]] || [ -f "${REPO_UPDATED_FILE}" ]; then
      log "INFO" "Install crun ${version} to ${INSTALLS_DIR}"
      [ -d "./bin" ] && rm -rf ./bin
      git checkout ${version}
      git submodule update --init --recursive
      git clean -d -f -f -x
      git submodule foreach --recursive git clean -d -f -f -x
      git submodule foreach --recursive git reset --hard
      ./autogen.sh
      local cflags="-s -fPIE -pie -Wall -Wextra"
      local ldflags=""
      local options=""
      local links=()
      if [ -d "${SYSTEM_INSTALLS}/wasmtime" ]; then
        cflags="${cflags} -I${SYSTEM_INSTALLS}/wasmtime/include"
        ldflags="${ldflags} -Wl,-rpath,${SYSTEM_INSTALLS}/wasmtime/lib"
        options="${options} --with-wasmtime"
        links+=('crun-wasmtime' 'crun-wasm')
      fi
      if [ -d "${SYSTEM_INSTALLS}/wasmedge" ]; then
        cflags="${cflags} -I${SYSTEM_INSTALLS}/wasmedge/include"
        ldflags="${ldflags} -Wl,-rpath,${SYSTEM_INSTALLS}/wasmedge/lib64"
        options="${options} --with-wasmedge"
        links+=('crun-wasmedge' 'crun-wasm')
      fi
      if [ -d "${SYSTEM_INSTALLS}/wasmer" ]; then
        cflags="${cflags} -I${SYSTEM_INSTALLS}/wasmer/include"
        ldflags="${ldflags} -Wl,-rpath,${SYSTEM_INSTALLS}/wasmer/lib"
        options="${options} --with-wasmer"
        links+=('crun-wasmer' 'crun-wasm')
      fi
      if [ -d "${SYSTEM_INSTALLS}/spin" ]; then
        options="${options} --with-spin"
        links+=('crun-spin')
      fi
      if [ -d "${SYSTEM_INSTALLS}/libkrun" ]; then
        cflags="${cflags} -I${SYSTEM_INSTALLS}/libkrun/include"
        ldflags="${ldflags} -Wl,-rpath,${SYSTEM_INSTALLS}/libkrun/lib64"
        options="${options} --with-libkrun"
        links+=('krun')
      fi
      CFLAGS="${cflags}" LDFLAGS="${ldflags}" ./configure --prefix="${INSTALLS_DIR:?}" ${options}
      make -j$(nproc)
      rm -rf "${INSTALLS_DIR:?}" || true
      find "${SYSTEM_INSTALLS_BIN}/" -xtype l -delete
      make install
      chmod -R ug=rwX,o=rX "${INSTALLS_DIR}"
      cp --archive --recursive --symbolic-link "${INSTALLS_DIR}"/bin/crun "${SYSTEM_INSTALLS_BIN}"
      for link in "${links[@]}"; do
        ln -sf "${INSTALLS_DIR}"/bin/crun "${SYSTEM_INSTALLS_BIN}/${link}"
      done
      restorecon -RvF "${SYSTEM_INSTALLS}"
      echo "${version}" >| "${VERSION_FILE}"
    fi
  )
}

if [ -d "${SYSTEM_INSTALLS}" ]; then
  ensure_crun_is_installed_from_source
fi
