#!/usr/bin/env bash
set -e
SCRIPT_DIR="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")"
ROOT_DIR="$(readlink -f "${SCRIPT_DIR}/../../..")"
source "${ROOT_DIR}/function.log.sh"
source "${ROOT_DIR}/env.sh"

BUILD_DIR="${SCRIPT_DIR}/build"
INSTALLS_DIR="${SYSTEM_INSTALLS}/containerd-shims"
VERSION_FILE="${INSTALLS_DIR}/version"

function ensure_containerd_shim_runwasi_is_installed_with_cargo () {
  # runwasi (https://github.com/containerd/runwasi)
  # requires: protobuf-compiler libseccomp-devel
  # https://docs.docker.com/engine/alternative-runtimes/#wasmtime
  # see also: 
  #  https://github.com/deislabs/containerd-wasm-shims
  #  https://github.com/spinkube/containerd-shim-spin
  local name="runwasi"
  local build_dir="${BUILD_DIR}/${name}"
  local install_dir="${INSTALLS_DIR}/${name}"
  local version_file="${VERSION_FILE}_${name}"
  if [ ! -d "${build_dir}" ]; then
      git clone https://github.com/containerd/runwasi.git "${build_dir}"
  fi
  pushd "${build_dir}" && git reset --hard HEAD && git checkout main && git pull
  local version="$(git rev-parse --short=7 HEAD)"

  if [ ! -f "${version_file}" ] || [[ ! $(cat "${version_file}") =~ ^${version}$ ]]; then
    log "INFO" "Install containerd shim ${name} ${version} to ${install_dir}"
    git clean -d -f -f -x
    rm -rf "${install_dir:?}" || true
    find "${SYSTEM_INSTALLS_BIN}/" -xtype l -delete
    mkdir -p "${install_dir:?}/bin"
    cargo build --release -p containerd-shim-wasmtime -p containerd-shim-wasmer -p containerd-shim-wasmedge -p oci-tar-builder
    cp target/release/containerd-shim-{wasmtime,wasmer,wasmedge}-v1 "${install_dir}/bin/"
    cp target/release/oci-tar-builder "${install_dir}/bin/"
    ln -srf ${install_dir}/bin/containerd-shim-wasmtime-v1 ${install_dir}/bin/containerd-shim-wasmtimed-v1
    ln -srf ${install_dir}/bin/containerd-shim-wasmtime-v1 ${install_dir}/bin/containerd-wasmtimed
    ln -srf ${install_dir}/bin/containerd-shim-wasmer-v1 ${install_dir}/bin/containerd-shim-wasmerd-v1
    ln -srf ${install_dir}/bin/containerd-shim-wasmer-v1 ${install_dir}/bin/containerd-wasmerd
    ln -srf ${install_dir}/bin/containerd-shim-wasmedge-v1 ${install_dir}/bin/containerd-shim-wasmedged-v1
    ln -srf ${install_dir}/bin/containerd-shim-wasmedge-v1 ${install_dir}/bin/containerd-wasmedged
    chmod -R ug=rwX,o=rX "${install_dir}"
    cp --archive --recursive --symbolic-link --force "${install_dir}"/bin/* "${SYSTEM_INSTALLS_BIN}"
    restorecon -RvF "${SYSTEM_INSTALLS}"
    echo "${version}" >| "${version_file}"
  fi
  popd
}

function ensure_containerd_shim_spinkube_spin_is_installed_with_cargo () {
  # containerd-shim-spin (https://github.com/spinkube/containerd-shim-spin)
  # requires (rpms): protobuf-compiler libseccomp-devel pkg-config openssl-devel
  # requires (perl): FindBin IPC::Cmd
  local name="spinkube-spin"
  local build_dir="${BUILD_DIR}/${name}"
  local install_dir="${INSTALLS_DIR}/${name}"
  local version_file="${VERSION_FILE}_${name}"
  if [ ! -d "${build_dir}" ]; then
      git clone https://github.com/spinkube/containerd-shim-spin.git "${build_dir}"
  fi
  pushd "${build_dir}" && git reset --hard HEAD && git checkout main && git remote prune origin && git pull
  local version="$(git rev-parse --short=7 HEAD)"

  if [ ! -f "${version_file}" ] || [[ ! $(cat "${version_file}") =~ ^${version}$ ]]; then
    log "INFO" "Install containerd shim ${name} ${version} to ${install_dir}"
    git clean -d -f -f -x
    mkdir -p "${install_dir:?}/bin"
    cargo install --root "${install_dir}" --path containerd-shim-spin --locked
    chmod -R ug+rwX,o+rX "${install_dir}"
    find "${SYSTEM_INSTALLS_BIN}/" -xtype l -delete
    cp --archive --recursive --symbolic-link --force "${install_dir}"/bin/* "${SYSTEM_INSTALLS_BIN}"
    restorecon -RvF "${SYSTEM_INSTALLS}"
    echo "${version}" >| "${version_file}"
  fi
  popd
}

if [ -d "${SYSTEM_INSTALLS}" ]; then
  ensure_containerd_shim_runwasi_is_installed_with_cargo
  ensure_containerd_shim_spinkube_spin_is_installed_with_cargo
fi
