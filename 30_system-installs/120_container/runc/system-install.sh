#!/usr/bin/env bash
set -e
SCRIPT_DIR="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")"
ROOT_DIR="$(readlink -f "${SCRIPT_DIR}/../../..")"
source "${ROOT_DIR}/function.log.sh"
source "${ROOT_DIR}/env.sh"

function ensure_selinux_label_set () {
  log "INFO" "Set SELinux labels for runc"
  local runc_pattern="${SYSTEM_INSTALLS}(/runc)?/bin/runc"
  (semanage fcontext -l | grep "${runc_pattern}") || semanage fcontext -a -t container_runtime_exec_t "${runc_pattern}"
  restorecon -RvF "${SYSTEM_INSTALLS}/runc/bin/runc"
  restorecon -RvF "${SYSTEM_INSTALLS}/bin/runc"
}

if [ -z "${SUDO_USER}" ]; then
  echo "The script must be executed with sudo!"
elif [ -d "${SYSTEM_INSTALLS}" ]; then
  ensure_selinux_label_set
fi
