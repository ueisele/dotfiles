#!/usr/bin/env bash
set -e
SCRIPT_DIR="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")"
ROOT_DIR="$(readlink -f "${SCRIPT_DIR}/../../..")"
source "${ROOT_DIR}/function.log.sh"
source "${ROOT_DIR}/env.sh"

INSTALLS_DIR="${SYSTEM_INSTALLS}/runc"
VERSION_FILE="${INSTALLS_DIR}/version"

function ensure_runc_is_installed () {
  # https://github.com/opencontainers/runc/releases/tag/v1.2.0-rc.1
  local curl_opts=(-fsSL)
  if [ -n "${GITHUB_API_TOKEN:-}" ]; then
    curl_opts=("${curl_opts[@]}" -H "Authorization: token $GITHUB_API_TOKEN")
  fi
  local version="$(curl "${curl_opts[@]}" https://api.github.com/repos/opencontainers/runc/releases | jq -r '.[].tag_name' | sed '/-/!{s/$/_/}' | sort --version-sort -r | sed 's/_$//' | head -n1)"
  local url="https://github.com/opencontainers/runc/releases/download/${version}/runc.amd64"

  if [ ! -f "${VERSION_FILE}" ] || [[ ! $(cat "${VERSION_FILE}") =~ ^${version}$ ]]; then
    log "INFO" "Install containerd ${version} to ${INSTALLS_DIR}"
    rm -rf "${INSTALLS_DIR:?}"/bin/* || true
    find "${SYSTEM_INSTALLS_BIN}/" -xtype l -delete
    mkdir -p "${INSTALLS_DIR}/bin"
    curl "${curl_opts[@]}" -o "${INSTALLS_DIR}/bin/runc" "${url}"
    chmod +x "${INSTALLS_DIR}/bin/runc"
    chmod -R ug=rwX,o=rX "${INSTALLS_DIR}"
    cp --archive --recursive --symbolic-link "${INSTALLS_DIR}"/bin/* "${SYSTEM_INSTALLS_BIN}"
    restorecon -RvF "${SYSTEM_INSTALLS}"
    echo "${version}" >| "${VERSION_FILE}"
  fi
}

if [ -d "${SYSTEM_INSTALLS}" ]; then
  ensure_runc_is_installed
fi
