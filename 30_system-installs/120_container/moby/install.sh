#!/usr/bin/env bash
set -e
SCRIPT_DIR="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")"
ROOT_DIR="$(readlink -f "${SCRIPT_DIR}/../../..")"
source "${ROOT_DIR}/function.log.sh"
source "${ROOT_DIR}/env.sh"

INSTALLS_DIR="${SYSTEM_INSTALLS}/moby"
VERSION_FILE="${INSTALLS_DIR}/version"

function ensure_moby_is_installed_from_source () {
  # https://github.com/moby/moby
  log "INFO" "Install moby with go"
  local build_dir="${SCRIPT_DIR}/build"
  if [ ! -d "${build_dir}" ]; then
      git clone https://github.com/moby/moby.git "${build_dir}"
  fi
  (
    cd "${build_dir}" && git reset --hard HEAD && git checkout master && git pull
    local version="$(git tag --sort=-version:refname | grep "^v[0-9\.]\+$" | head -n 1)"
    if [ ! -f "${VERSION_FILE}" ] || [[ ! $(cat "${VERSION_FILE}") =~ ^${version}$ ]]; then
      log "INFO" "Install moby ${version} to ${INSTALLS_DIR}"
      [ -d "./bin" ] && rm -rf ./bin
      git checkout ${version}
      git clean -d -f -f -x
      local pkg="github.com/docker/docker"
      local revision="$(git rev-parse HEAD)"
      local buildtime="$(date -u -d "@$(date +%s)" --rfc-3339 ns 2> /dev/null | sed -e 's/ /T/')"
      local ldflags="\
      -X \"${pkg}/dockerversion.Version=${version}\" \
      -X \"${pkg}/dockerversion.GitCommit=${revision}\" \
      -X \"${pkg}/dockerversion.BuildTime=${buildtime}\" \
      -X \"${pkg}/dockerversion.PlatformName=Docker Engine - Community\" \
      -X \"${pkg}/dockerversion.ProductName=Docker Daemon\" \
      -X \"${pkg}/dockerversion.DefaultProductLicense=Community Engine\" "
      # build dockerd
      CGO_ENABLED=1 ./hack/with-go-mod.sh go build -mod=vendor -modfile=vendor.mod -ldflags "${ldflags} -s -w -extldflags -static" -tags "netgo osusergo static_build journald"  -o "./bin/dockerd" "./cmd/dockerd"
      # build docker-proxy
      CGO_ENABLED=1 ./hack/with-go-mod.sh go build -mod=vendor -modfile=vendor.mod -ldflags "${ldflags} -s -w -extldflags -static" -tags "netgo osusergo static_build journald"  -o "./bin/docker-proxy" "./cmd/docker-proxy"
      # install
      rm -f "${INSTALLS_DIR:?}"/bin/* || true
      find "${SYSTEM_INSTALLS_BIN}/" -xtype l -delete
      mkdir -p "${INSTALLS_DIR}"/bin
      cp bin/* "${INSTALLS_DIR}"/bin
      chmod -R ug=rwX,o=rX "${INSTALLS_DIR}/bin"
      cp --archive --recursive --symbolic-link "${INSTALLS_DIR}"/bin/{dockerd,docker-proxy} "${SYSTEM_INSTALLS_BIN}"
      restorecon -RvF "${SYSTEM_INSTALLS}"
      echo "${version}" >| "${VERSION_FILE}"
    fi
  )
}

if [ -d "${SYSTEM_INSTALLS}" ]; then
  ensure_moby_is_installed_from_source
fi
