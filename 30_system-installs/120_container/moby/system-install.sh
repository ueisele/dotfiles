#!/usr/bin/env bash
set -e
SCRIPT_DIR="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")"
ROOT_DIR="$(readlink -f "${SCRIPT_DIR}/../../..")"
source "${ROOT_DIR}/function.log.sh"
source "${ROOT_DIR}/env.sh"

function ensure_docker_group_exists () {
  getent group docker || groupadd --gid 2000 --system --users "${SUDO_USER}" docker
}

function ensure_data_dir_exists () {
  local data_dir="/var/lib/docker"
  log "INFO" "Ensure data dir ${data_dir} exists"
  if [ ! -d "${data_dir}" ]; then
    mkdir -p ${data_dir}
    chmod -R u=rwX,go= ${data_dir}
    chown -R root:docker ${data_dir}
    restorecon -RvF ${data_dir}
  fi
}

function ensure_selinux_label_set () {
  log "INFO" "Set SELinux labels for docker"
  local docker_pattern="${SYSTEM_INSTALLS}(/moby)?/bin/dockerd.*"
  (semanage fcontext -l | grep "${docker_pattern}") || semanage fcontext -a -t container_runtime_exec_t "${docker_pattern}"
  restorecon -RvF "${SYSTEM_INSTALLS}/moby/bin"
  restorecon -RvF "${SYSTEM_INSTALLS}/bin"
}

function ensure_docker_config_is_installed () {
  log "INFO" "Install docker config"
  mkdir -p /etc/docker/certs.d
  \cp -f "${SCRIPT_DIR}/system-files/etc/docker/daemon.json" /etc/docker/
  chmod -R u=rwX,go=rX /etc/docker
  chown -R root:root /etc/docker
}

function ensure_systemd_service_is_enabled () {
  log "INFO" "Enable docker systemd service"
  \cp -f "${SCRIPT_DIR}"/system-files/etc/systemd/system/docker.{service,socket} /etc/systemd/system/
  chmod u=rwX,go=rX /etc/systemd/system/docker.{service,socket}
  chown root:root /etc/systemd/system/docker.{service,socket}
  systemctl daemon-reload
  systemctl enable docker.socket
  systemctl start docker.socket
}

if [ -z "${SUDO_USER}" ]; then
  echo "The script must be executed with sudo!"
elif [ -d "${SYSTEM_INSTALLS}" ]; then
  ensure_docker_group_exists
  ensure_data_dir_exists
  ensure_selinux_label_set
  ensure_docker_config_is_installed
  ensure_systemd_service_is_enabled
fi
