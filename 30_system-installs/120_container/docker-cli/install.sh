#!/usr/bin/env bash
set -e
SCRIPT_DIR="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")"
ROOT_DIR="$(readlink -f "${SCRIPT_DIR}/../../..")"
source "${ROOT_DIR}/function.log.sh"
source "${ROOT_DIR}/env.sh"

INSTALLS_DIR="${SYSTEM_INSTALLS}/docker-cli"
VERSION_FILE="${INSTALLS_DIR}/version"

PLUGINS_INSTALLS_DIR="${INSTALLS_DIR}/cli-plugins"
PLUGINS_VERSION_FILE_PREFIX="${INSTALLS_DIR}/cli-plugin-version-"

function ensure_docker_cli_is_installed_from_source () {
  # https://github.com/docker/cli
  log "INFO" "Install docker-cli with go"
  local build_dir="${SCRIPT_DIR}/build"
  if [ ! -d "${build_dir}" ]; then
      git clone https://github.com/docker/cli.git "${build_dir}"
  fi
  (
    cd "${build_dir}" && git reset --hard HEAD && git checkout master && git pull
    local version="$(git tag --sort=-version:refname | grep "^v[0-9\.]\+$" | head -n 1)"
    if [ ! -f "${VERSION_FILE}" ] || [[ ! $(cat "${VERSION_FILE}") =~ ^${version}$ ]]; then
      log "INFO" "Install docker-cli ${version} to ${INSTALLS_DIR}"
      [ -d "./bin" ] && rm -rf ./bin
      git checkout ${version}
      git clean -d -f -f -x
      mv vendor.mod go.mod
      local pkg="github.com/docker/cli/cli"
      local revision="$(git rev-parse HEAD)"
      local buildtime="$(date -u -d "@$(date +%s)" --rfc-3339 ns 2> /dev/null | sed -e 's/ /T/')"
      local ldflags="\
      -X \"${pkg}/version.Version=${version}\" \
      -X \"${pkg}/version.GitCommit=${revision}\" \
      -X \"${pkg}/version.BuildTime=${buildtime}\" \
      -X \"${pkg}/version.PlatformName=Docker Engine - Community\" "
      # build docker
      CGO_ENABLED=1 go build -ldflags "${ldflags} -s -w -extldflags -static" -tags "netgo osusergo static_build pkcs11" -o "./bin/docker" "./cmd/docker"
      # install
      rm -f "${INSTALLS_DIR:?}"/bin/* || true
      find "${SYSTEM_INSTALLS_BIN}/" -xtype l -delete
      mkdir -p "${INSTALLS_DIR}"/bin
      cp bin/* "${INSTALLS_DIR}"/bin
      chmod -R ug=rwX,o=rX "${INSTALLS_DIR}/bin"
      cp --archive --recursive --symbolic-link "${INSTALLS_DIR}"/bin/docker "${SYSTEM_INSTALLS_BIN}"
      restorecon -RvF "${SYSTEM_INSTALLS}"
      echo "${version}" >| "${VERSION_FILE}"
    fi
  )
}

function ensure_cli_plugin_buildx_is_installed () {
  # https://github.com/docker/buildx/
  local curl_opts=(-fsSL)
  if [ -n "${GITHUB_API_TOKEN:-}" ]; then
    curl_opts=("${curl_opts[@]}" -H "Authorization: token $GITHUB_API_TOKEN")
  fi
  local plugin_name="buildx"
  local version="$(curl "${curl_opts[@]}" https://api.github.com/repos/docker/buildx/releases | jq -r '.[].tag_name' | sed '/-/!{s/$/_/}' | sort --version-sort -r | sed 's/_$//' | head -n1)"
  local url="https://github.com/docker/buildx/releases/download/${version}/buildx-${version}.linux-amd64"
  local version_file="${PLUGINS_VERSION_FILE_PREFIX}${plugin_name}"
  if [ ! -f "${version_file}" ] || [[ ! $(cat "${version_file}") =~ ^${version}$ ]]; then
    local install_file="${PLUGINS_INSTALLS_DIR}/docker-${plugin_name}"
    log "INFO" "Install Docker cli plugin ${plugin_name} ${version} to ${install_file}"
    mkdir -p "${PLUGINS_INSTALLS_DIR}"
    [ -e "${install_file}" ] && rm -f "${install_file}"
    curl "${curl_opts[@]}" -o "${install_file}" "${url}"
    chmod +x "${install_file}"
    chmod -R ug=rwX,o=rX "${install_file}"
    restorecon -RvF "${install_file}"
    echo "${version}" >| "${version_file}"
  fi
}

function ensure_cli_plugin_compose_is_installed () {
  # https://github.com/docker/compose/
  local curl_opts=(-fsSL)
  if [ -n "${GITHUB_API_TOKEN:-}" ]; then
    curl_opts=("${curl_opts[@]}" -H "Authorization: token $GITHUB_API_TOKEN")
  fi
  local plugin_name="compose"
  local version="$(curl "${curl_opts[@]}" https://api.github.com/repos/docker/compose/releases | jq -r '.[].tag_name' | sed '/-/!{s/$/_/}' | sort --version-sort -r | sed 's/_$//' | head -n1)"
  local url="https://github.com/docker/compose/releases/download/${version}/docker-compose-linux-x86_64"
  local version_file="${PLUGINS_VERSION_FILE_PREFIX}${plugin_name}"
  if [ ! -f "${version_file}" ] || [[ ! $(cat "${version_file}") =~ ^${version}$ ]]; then
    local install_file="${PLUGINS_INSTALLS_DIR}/docker-${plugin_name}"
    log "INFO" "Install Docker cli plugin ${plugin_name} ${version} to ${install_file}"
    mkdir -p "${PLUGINS_INSTALLS_DIR}"
    [ -e "${install_file}" ] && rm -f "${install_file}"
    curl "${curl_opts[@]}" -o "${install_file}" "${url}"
    chmod +x "${install_file}"
    chmod -R ug=rwX,o=rX "${install_file}"
    restorecon -RvF "${install_file}"
    echo "${version}" >| "${version_file}"
  fi
}

function ensure_shell_completion_is_created () {
  log "INFO" "Shell completion for docker"
  "${INSTALLS_DIR}"/bin/docker completion zsh >| "${HOME}/.zsh/zfunc.d/_docker"
}


if [ -d "${SYSTEM_INSTALLS}" ]; then
  ensure_docker_cli_is_installed_from_source
  ensure_cli_plugin_buildx_is_installed
  ensure_cli_plugin_compose_is_installed
  ensure_shell_completion_is_created
fi
