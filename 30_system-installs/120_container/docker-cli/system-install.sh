#!/usr/bin/env bash
set -e
SCRIPT_DIR="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")"
ROOT_DIR="$(readlink -f "${SCRIPT_DIR}/../../..")"
source "${ROOT_DIR}/function.log.sh"
source "${ROOT_DIR}/env.sh"

function ensure_cli_plugins_are_linked () {
  local docker_libexec_dir="/usr/local/libexec/docker"
  log "INFO" "Ensure Docker cli libexec dir ${docker_libexec_dir} exists"
  if [ ! -d "${docker_libexec_dir}" ]; then
    mkdir -p ${docker_libexec_dir}
    chmod -R u=rwX,go=rX ${docker_libexec_dir}
    chown -R root:root ${docker_libexec_dir}
    restorecon -RvF ${docker_libexec_dir}
  fi
  log "INFO" "Ensure Docker cli plugins are linked to ${docker_libexec_dir}/cli-plugins"
  ln -sf "${SYSTEM_INSTALLS}/docker-cli/cli-plugins" "${docker_libexec_dir}/"
}

if [ -z "${SUDO_USER}" ]; then
  echo "The script must be executed with sudo!"
elif [ -d "${SYSTEM_INSTALLS}" ]; then
  ensure_cli_plugins_are_linked
fi
