#
# Configures mise tools
#

# Return if requirements are not found
if [ ! -f "${HOME}/.local/bin/mise" ]; then
    return 0
fi

# gcloud
if mise where gcloud > /dev/null 2>&1; then
  path=(
    $(mise where gcloud)/bin
    $path
  )
fi

# micromamba
if mise where micromamba > /dev/null 2>&1; then
  export MAMBA_ROOT_PREFIX="${HOME}/.micromamba"
  export CONDA_ROOT_PREFIX="${MAMBA_ROOT_PREFIX}"
  source <(mise exec micromamba -- micromamba shell hook --shell zsh)
fi

# bat
if mise where bat > /dev/null 2>&1; then
  alias cat='bat --paging=never'
fi
