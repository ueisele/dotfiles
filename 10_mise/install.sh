#!/usr/bin/env bash
set -e
SCRIPT_DIR="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")"
ROOT_DIR="$(readlink -f "${SCRIPT_DIR}/..")"
source "${ROOT_DIR}/function.log.sh"

MISE_BIN="${HOME}/.local/bin/mise"

function ensure_update_script_is_linked () {
    log "INFO" "Linking mise update script to ${HOME}"
    ln -sf "${SCRIPT_DIR}/install.sh" "${HOME}/.local/bin/update-mise"
}

function ensure_mise_is_installed_from_source () {
  # https://github.com/jdx/mise
  log "INFO" "Installing mise with Rust to ${MISE_BIN}"
  local build_dir="${SCRIPT_DIR}/build"
  if [ ! -d "${build_dir}" ]; then
      git clone https://github.com/jdx/mise.git "${build_dir}"
  fi
  (
    cd "${build_dir}" && git reset --hard HEAD && git checkout main && git pull
    local version="$(git tag --sort=-version:refname | grep "^v[0-9\.]\+$" | head -n 1)"
    if [ ! -e "${MISE_BIN}" ] || [[ ! "v$("${MISE_BIN}" --version | awk '{print $1}')" =~ ^${version}$ ]]; then
      log "INFO" "Install mise ${version} to ${MISE_BIN}"
      git checkout "${version}"
      git clean -d -f -f -x
      # apply patches
      if [[ -d "${SCRIPT_DIR}/patches/" ]]; then
        for patch_file in "${SCRIPT_DIR}"/patches/*.patch; do
          patch -p1 < "${patch_file}"
        done
      fi
      # build
      cargo build --release --locked
      # install
      if [ -e "${MISE_BIN}" ]; then
        rm -f "${MISE_BIN:?}"
      fi
      mkdir -p "$(dirname "${MISE_BIN}")"
      cp ./target/release/mise "${MISE_BIN}"
      mkdir -p "${HOME}"/.zsh/{zshrc,zfunc}.d/
      "${MISE_BIN}" activate zsh >| "${HOME}/.zsh/zshrc.d/mise-00-init.zsh"
      "${MISE_BIN}" completion zsh >| "${HOME}/.zsh/zfunc.d/_mise"
      git clean -d -f -f -x
    fi
  )
}

function ensure_dotfiles_are_linked () {
    log "INFO" "Linking mise dotfiles to ${HOME}"
    "${ROOT_DIR}/tool.link-files.sh" "${SCRIPT_DIR}/files"
}

function ensure_asdf_compat_is_configured () {
    log "INFO" "Ensure mise is compatible with asdf"
    mkdir -p "${HOME}/.local/share/mise"
    ln -sf "${HOME}/.local/share/mise" "${HOME}/.asdf"
    ln -sf "${HOME}/.local/share/mise/.fake-asdf/asdf" "${HOME}/.local/bin/asdf"
}

function ensure_mise_plugins_are_updated () {
    log "INFO" "Update mise plugins"
    "${MISE_BIN}" plugins update -y
}

function ensure_mise_tools_are_updated () {
    log "INFO" "Update tools installed with mise"
    "${MISE_BIN}" upgrade -y
    "${MISE_BIN}" prune -y
}

#function ensure_default_packages_installed_for_poetry () {
#  if "${MISE_BIN}" where pipx:poetry > /dev/null 2>&1; then
#    for poetryversion in $("${MISE_BIN}" list pipx:poetry --no-headers | awk '{print $2}'); do
#      echo "Installing default packages for poetry ${poetryversion#\*}."
#      "${MISE_BIN}" exec pipx:poetry@${poetryversion#\*} -- poetry self add poetry-multiproject-plugin@latest
#    done
#  fi
#}

function _main () {
    ensure_update_script_is_linked
    ensure_mise_is_installed_from_source
    ensure_dotfiles_are_linked
    ensure_asdf_compat_is_configured

    ensure_mise_plugins_are_updated
    ensure_mise_tools_are_updated

    #ensure_default_packages_installed_for_poetry
}

if [ "${BASH_SOURCE[0]}" == "$0" ]; then
    _main "$@"
fi