#!/usr/bin/env bash
set -e
SCRIPT_DIR="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")"
ROOT_DIR="$(readlink -f "${SCRIPT_DIR}/../../..")"
source "${ROOT_DIR}/function.log.sh"

function ensure_desktop_entry_exists () {
    log "INFO" "Linking VIA Desktop Entry files to ${HOME}"
	  "${ROOT_DIR}/tool.link-files.sh" "${SCRIPT_DIR}/files"
    update-desktop-database ~/.local/share/applications
}

ensure_desktop_entry_exists
