#!/usr/bin/env bash
set -e
SCRIPT_DIR="$(dirname $(readlink -f ${BASH_SOURCE[0]}))"
ROOT_DIR="$(readlink -f ${SCRIPT_DIR}/../..)"
REPOSITORIES_DIR="$(readlink -f "${SCRIPT_DIR}/repositories")"
MARKERS_DIR="$(readlink -f "${SCRIPT_DIR}/markers")"
source "${ROOT_DIR}/function.log.sh"

function ensure_install_script_is_linked () {
  log "INFO" "Linking Gnome Shell extensions update script to ${HOME}"
  ln -sf "${SCRIPT_DIR}/install.sh" "${HOME}/.local/bin/update-gnome-shell-extensions"
}

function ensure_pop_shell_is_installed () {
    if [ ! -d "${REPOSITORIES_DIR}/pop-shell" ]; then
        log "INFO" "Cloning Pop Shell GitHub Repository to ${REPOSITORIES_DIR}/pop-shell"
        git clone https://github.com/pop-os/shell.git "${REPOSITORIES_DIR}/pop-shell"       
    fi
    log "INFO" "Updating Pop Shell GitHub Repository in ${REPOSITORIES_DIR}/pop-shell"
    (cd "${REPOSITORIES_DIR}/pop-shell" && git pull && make depcheck clean compile install)
    if [ ! -f "${MARKERS_DIR}/pop-shell/configured" ]; then
      (cd "${REPOSITORIES_DIR}/pop-shell" && make configure)
      mkdir -p "${MARKERS_DIR}/pop-shell"
      touch "${MARKERS_DIR}/pop-shell/configured"
    fi
    gnome-extensions enable pop-shell@system76.com || true
}

function ensure_ddterm_is_installed () {
    if [ ! -d "${REPOSITORIES_DIR}/ddterm" ]; then
        log "INFO" "Cloning ddterm GitHub Repository to ${REPOSITORIES_DIR}/ddterm"
        git clone https://github.com/ddterm/gnome-shell-extension-ddterm.git "${REPOSITORIES_DIR}/ddterm"       
    fi
    log "INFO" "Updating ddterm GitHub Repository in ${REPOSITORIES_DIR}/ddterm"
    (cd "${REPOSITORIES_DIR}/ddterm" && git pull && ./do-in-podman.sh meson setup build-dir && ./do-in-podman.sh ninja -C build-dir pack && gnome-extensions install -f build-dir/ddterm@amezin.github.com.shell-extension.zip)
    gnome-extensions enable ddterm@amezin.github.com || true
}

function ensure_dash_to_panel_is_installed () {
    if [ ! -d "${REPOSITORIES_DIR}/dash-to-panel" ]; then
        log "INFO" "Cloning Dash-to-Panel GitHub Repository to ${REPOSITORIES_DIR}/dash-to-panel"
        git clone https://github.com/home-sweet-gnome/dash-to-panel.git "${REPOSITORIES_DIR}/dash-to-panel"       
    fi
    log "INFO" "Updating Dash-to-Panel GitHub Repository in ${REPOSITORIES_DIR}/dash-to-panel"
    (cd "${REPOSITORIES_DIR}/dash-to-panel" && git pull && make clean zip-file && gnome-extensions install -f dash-to-panel@jderose9.github.com.zip)
    gnome-extensions enable dash-to-panel@jderose9.github.com || true
}

ensure_install_script_is_linked
ensure_pop_shell_is_installed
ensure_ddterm_is_installed
ensure_dash_to_panel_is_installed
