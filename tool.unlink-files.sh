#!/usr/bin/env bash
set -e
SCRIPT_DIR="$(dirname $(readlink -f ${BASH_SOURCE[0]}))"
ROOT_DIR="$(readlink -f ${SCRIPT_DIR})"
source ${ROOT_DIR}/function.log.sh

function unlink_dotfiles_in_dir () {
    local sourcedir="${1:?Missing source dir as first parameter!}"
    local targetdir="${2:-"${HOME}"}"
    for file in $(find "${sourcedir}" -not -name '*.btpl' -not -type d); do
        unlink_dotfile "${file}" "${sourcedir}" "${targetdir}"
    done
}

function unlink_dotfile () {
    local sourcefile="${1:?Missing source file as first parameter!}"
    local sourcedir="${2:-"$(dirname "${sourcefile}")"}"
    local targetdir="${3:-"${HOME}"}"
    local relfile="$(realpath --relative-to=${sourcedir} ${sourcefile})"
    if [ -h "${targetdir}/${relfile}" ]; then
      log "INFO" "Removing symlink ${targetdir}/${relfile}, pointing to $(realpath ${sourcefile})"
      rm -f "${targetdir}/${relfile}"
    fi
}

function _main () {
    local source="${1:?Missing source as first parameter!}"
    if [ -d "${source}" ]; then
        unlink_dotfiles_in_dir "$@"
    else
        unlink_dotfile "$@"
    fi
}

if [ "${BASH_SOURCE[0]}" == "$0" ]; then
    _main "$@"
fi