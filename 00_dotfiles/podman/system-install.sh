#!/usr/bin/env bash
set -e
SCRIPT_DIR="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")"
ROOT_DIR="$(readlink -f "${SCRIPT_DIR}/../..")"
source "${ROOT_DIR}/function.log.sh"
source "${ROOT_DIR}/env.sh"

function ensure_podman_configs_are_installed () {
  log "INFO" "Install Podman configs"
  local config_dir=/etc/containers
  if [ ! -d "${config_dir}" ]; then
    mkdir -p "${config_dir}"
    chmod -R u=rwX,go=rX "${config_dir}"
    chown -R root:root "${config_dir}"
  fi
  \cp -f "${SCRIPT_DIR}"/system-files"${config_dir}"/{containers,storage}.conf "${config_dir}"/
  chmod -R u=rwX,go=rX "${config_dir}"/{containers,storage}.conf
  chown -R root:root "${config_dir}"/{containers,storage}.conf
}

if [ -z "${SUDO_USER}" ]; then
  echo "The script must be executed with sudo!"
elif [ -d "${SYSTEM_INSTALLS}" ]; then
  ensure_podman_configs_are_installed
fi
