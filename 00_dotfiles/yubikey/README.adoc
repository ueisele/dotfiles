= Yubikey

https://www.evernote.com/shard/s637/nl/187613238/cf7f43ab-b4be-1160-7808-48d4e3273364

== Prerequisites

If you did not bootstrap this repository with link:../../bootstrap.sh[bootstrap.sh] run the following commands first.

.Create the .ssh directory:
[source,bash]
----
mkdir -p ~/.ssh/
chmod 700 ~/.ssh/
----

.Create directory for ssh keys on Yubikeys:
[source,bash]
----
mkdir -p ~/.ssh-yubikey/
chmod 700 ~/.ssh-yubikey/
cd ~/.ssh-yubikey
----

.For each Yubikey you have do:
[source,bash]
----
ssh-keygen -K -N ''
----

== GnuPG

=== Extend expiration of public keys

.Create temporary GnuPG home:
[source,bash]
----
export GNUPGHOME=/tmp/.gnupg-tmp
mkdir -p "${GNUPGHOME}"
chmod 700 "${GNUPGHOME}"
sudo systemctl restart pcscd.service
----

.Navigate to the directory with the following files and run:
[source,bash]
----
gpg --import public-keys.asc
gpg --import private-keys.asc
gpg --import private-subkeys.asc
gpg --import-ownertrust ownertrust.asc
----

.Open the edit shell for your master key:
[source,bash]
----
MASTER_KEY_ID="$(gpg --list-secret-keys --keyid-format long --with-colons | awk -F: '$1 == "sec" {print $5}')"
gpg --edit-key ${MASTER_KEY_ID}
----

.Run the following commands to set the expiration date to master key and after this to the 3 sub keys:
[source,gnupg]
----
expire
key 1
key 2
key 3
expire
save
----

.Export public keys and ownertrust
[source,bash]
----
gpg --armor --export ${MASTER_KEY_ID} >| ~/.dotfiles/00_dotfiles/yubikey/gpg/public-keys.asc 
gpg --export-ownertrust >| ~/.dotfiles/00_dotfiles/yubikey/gpg/ownertrust.asc
----

.Delete temporary GnuPG home:
[source,bash]
----
rm -rf "${GNUPGHOME}"
unset "${GNUPGHOME}"
sudo systemctl restart pcscd.service
----

.Import public keys and ownertrust
[source,bash]
----
gpg --import ~/.dotfiles/00_dotfiles/yubikey/gpg/public-keys.asc 
gpg --import-ownertrust ~/.dotfiles/00_dotfiles/yubikey/gpg/ownertrust.asc
----

.Print your public key and replace it with your existing public key on GitHub and GitLab
[source,bash]
----
gpg --armor --export ${MASTER_KEY_ID}
----

* GitHub: https://github.com/settings/keys
* GitLab: https://gitlab.com/-/user_settings/gpg_keys
