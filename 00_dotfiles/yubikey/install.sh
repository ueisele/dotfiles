#!/usr/bin/env bash
set -e
SCRIPT_DIR="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")"
ROOT_DIR="$(readlink -f "${SCRIPT_DIR}/../..")"
source "${ROOT_DIR}/function.log.sh"

function ensure_gnupg_dir_exists () {
  log "INFO" "Ensure directory ${HOME}.gnupg exists"
	if [ ! -d "${HOME}/.gnupg" ]; then
    mkdir -p "${HOME}/.gnupg"
    chmod u=rwX,go= "${HOME}/.gnupg"
  fi
}

function ensure_dotfiles_are_linked () {
  log "INFO" "Linking Yubikey dotfiles to ${HOME}"
	${ROOT_DIR}/tool.link-files.sh "${SCRIPT_DIR}/files"
}

function ensure_gpg_keys_are_imported () {
  log "INFO" "Ensure GPG Public Keys are imported"
  gpg --import "${SCRIPT_DIR}/gpg/public-keys.asc"
  gpg --import-ownertrust "${SCRIPT_DIR}/gpg/ownertrust.asc"
}

ensure_gnupg_dir_exists
ensure_dotfiles_are_linked
ensure_gpg_keys_are_imported