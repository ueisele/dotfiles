#!/usr/bin/env bash
set -e
SCRIPT_DIR="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")"
ROOT_DIR="$(readlink -f "${SCRIPT_DIR}/../..")"
source "${ROOT_DIR}/function.log.sh"
source "${ROOT_DIR}/env.sh"

function ensure_system_udev_rules_exists () {
  log "INFO" "Ensure /etc/udev/rules.d/20-ueisele-yubikey.rules exists"
  if [ ! -d /etc/udev/rules.d/ ]; then
    mkdir -p /etc/udev/rules.d/
    chmod u=rwX,go=rX /etc/udev/rules.d/
    chown root:root /etc/udev/rules.d/
  fi
  \cp -f "${SCRIPT_DIR}/system-files/etc/udev/rules.d/20-ueisele-yubikey.rules" /etc/udev/rules.d/
  chmod u=rw,go=r /etc/udev/rules.d/20-ueisele-yubikey.rules
  chown root:root /etc/udev/rules.d/20-ueisele-yubikey.rules
  udevadm control --reload-rules
  udevadm trigger
}

if [ -z "${SUDO_USER}" ]; then
  echo "The script must be executed with sudo!"
elif [ -d "${SYSTEM_INSTALLS}" ]; then
  ensure_system_udev_rules_exists
fi
