#!/usr/bin/env bash
set -e
SCRIPT_DIR="$(dirname $(readlink -f ${BASH_SOURCE[0]}))"
ROOT_DIR="$(readlink -f ${SCRIPT_DIR}/../..)"
source ${ROOT_DIR}/function.log.sh

function ensure_dotfiles_are_linked () {
    log "INFO" "Linking Neovim dotfiles to ${HOME}"
	  ${ROOT_DIR}/tool.link-files.sh "${SCRIPT_DIR}/files"
}

function ensure_plugins_are_installed () {
    log "INFO" "Installing Neovim plugins"
	  nvim +'PlugInstall --sync' +qa
	  nvim +'PlugUpdate' +qa
}

ensure_dotfiles_are_linked
ensure_plugins_are_installed