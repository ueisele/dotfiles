#
# Defines environment variables.
#

#
# SHELL
#

export SHELL=$(command -v zsh)

#
# Editors
#

export PAGER=less
export MANPAGER="bat -p -l man"
export EDITOR=nvim 
export VISUAL=nvim

#
# Language
#

if ( [[ -z "${LANG}" ]] || [[ "${LANG}" =~ "C"* ]] ) && ( command -v locale > /dev/null ) && ( locale -a | grep -q en_US.utf8 ); then
  export LANG='en_US.utf8'
fi
if [[ -z "${LC_TIME}" ]] && command -v locale > /dev/null && locale -a | grep -q "^C$"; then
  export LC_TIME='C'
fi

#
# Timezone
#
export TZ=/usr/share/zoneinfo/Europe/Berlin

#
# Paths
#

# Ensure path arrays do not contain duplicates.
typeset -gU cdpath fpath mailpath path

# Set the list of directories that Zsh searches for programs.
path=(
  ${HOME}/.local/bin
  ${HOME}/bin
  $path
)

#
# Less
#

# Set the default Less options.
# Mouse-wheel scrolling has been disabled by -X (disable screen clearing).
# Remove -X and -F (exit if the content fits on one screen) to enable it.
export LESS='-F -g -i -M -R -S -w -X -z-4'

# Set the Less input preprocessor.
# Try both lesspipe and lesspipe.sh as either might exist on a system.
if (( $#commands[(i)lesspipe(|.sh)] )); then
  export LESSOPEN="| /usr/bin/env $commands[(i)lesspipe(|.sh)] %s 2>&-"
fi

#
# Load custom zshenv from ~/.zsh/zshenv.d
#
if [[ -d ${HOME}/.zsh/zshenv.d/ ]]; then
	for zshenv in ${HOME}/.zsh/zshenv.d/*.zsh(N); do
		test -r "$zshenv" && source "$zshenv"
	done
	unset zshenv
fi