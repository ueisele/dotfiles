#
# Executes commands at login pre-zshrc.
#

#
# Load ~/.profile or /etc/profile.d/
#
if [[ -f ${HOME}/.profile ]]; then 
	emulate sh -c 'source ${HOME}/.profile'
elif [[ -d /etc/profile.d/ ]]; then
	for profile in /etc/profile.d/*.sh(N); do
		test -r "$profile" && emulate sh -c "source $profile"
	done
	unset profile	
fi

#
# Load custom zprofile from ~/.zsh/zprofile.d
#
if [[ -d ${HOME}/.zsh/zprofile.d/ ]]; then
	for zprofile in ${HOME}/.zsh/zprofile.d/*.zsh(N); do
		test -r "$zprofile" && source "$zprofile"
	done
	unset zprofile
fi
