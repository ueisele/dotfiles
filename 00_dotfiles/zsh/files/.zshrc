#
# Executes commands at the start of an interactive session.
#

# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh

# Source Prezto.
if [[ -s "${ZDOTDIR:-$HOME}/.zprezto/init.zsh" ]]; then
  source "${ZDOTDIR:-$HOME}/.zprezto/init.zsh"
fi

#
# Load custom zshrc from ~/.zsh/zshrc.d
#
if [[ -d ${HOME}/.zsh/zshrc.d/ ]]; then
	for zshrc in ${HOME}/.zsh/zshrc.d/*.zsh(N); do
		test -r "$zshrc" && source "$zshrc"
	done
	unset zshrc
fi

#
# Load custom completions from ~/.zsh/zfunc.d
#
if [[ -d ${HOME}/.zsh/zfunc.d/ ]]; then
  # append completions to fpath
  fpath=(${HOME}/.zsh/zfunc.d $fpath)
  # initialise completions with ZSH's compinit
  autoload -Uz compinit && compinit
fi
