#!/usr/bin/env bash
set -e
SCRIPT_DIR="$(dirname $(readlink -f ${BASH_SOURCE[0]}))"
ROOT_DIR="$(readlink -f ${SCRIPT_DIR}/../..)"
source ${ROOT_DIR}/function.log.sh

function ensure_prezto_installed () {
    if [ ! -d "${HOME}/.zprezto" ]; then
        log "INFO" "Cloning Prezto GitHub Repository to ${HOME}/.zprezto"
        git clone --recursive --depth 1 --jobs 8 https://github.com/sorin-ionescu/prezto.git "${HOME}/.zprezto"
    else
        log "INFO" "Updating Prezto GitHub Repository in ${HOME}/.zprezto"
        (cd "${HOME}/.zprezto" && git pull && git submodule update --init --recursive)
    fi
}

function ensure_dotfiles_are_linked () {
    log "INFO" "Linking ZSH dotfiles to ${HOME}"
	${ROOT_DIR}/tool.link-files.sh "${SCRIPT_DIR}/files"
}

ensure_prezto_installed
ensure_dotfiles_are_linked