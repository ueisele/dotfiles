#!/usr/bin/env bash
set -e
SCRIPT_DIR="$(dirname $(readlink -f ${BASH_SOURCE[0]}))"
ROOT_DIR="$(readlink -f ${SCRIPT_DIR}/../..)"
source ${ROOT_DIR}/function.log.sh

function ensure_dotfiles_are_linked () {
    log "INFO" "Linking Git dotfiles to ${HOME}"
    ${ROOT_DIR}/tool.link-files.sh "${SCRIPT_DIR}/files"
}

function ensure_git_is_configured () {
    log "INFO" "Configuring Git"
    ## Init
    git config --global init.defaultBranch main

    ## User
    [[ -f "${HOME}/.git-env" ]] && source "${HOME}/.git-env"
    gitUserConfig "${HOME}/.gitconfig" "${GIT_USER_NAME:-$(whoami)}" "${GIT_USER_EMAIL:-$(whoami)@$(hostname)}"

    ## Core
    git config --global core.autocrlf input
    git config --global core.excludesfile '~/.gitignore-global'

    ## Pull
    git config --global pull.rebase true

    ## Push
    git config --global push.default simple

    ## Merge
    git config --global merge.conflictstyle diff3
    git config --global merge.tool bc3
    git config --global mergetool.bc3.trustExitCode true
    git config --global mergetool.prompt false
    git config --global mergetool.keepTemporaries false
    git config --global mergetool.keepBackup false
    
    ## Diff
    git config --global diff.tool bc3
    git config --global difftool.bc3.trustExitCode true

    ## Delta (https://github.com/dandavison/delta)
    git config --global core.pager delta
    git config --global interactive.diffFilter 'delta --color-only'
    git config --global delta.navigate true
    git config --global diff.colorMoved default
    
    ## Credential Helper
    git config --global credential.helper /usr/libexec/git-core/git-credential-libsecret

    ## Conditional Users
    sed -i '\:#START CONDITIONAL USER SECTION:,\:#END CONDITIONAL USER SECTION:d' "${HOME}/.gitconfig"
    echo "#START CONDITIONAL USER SECTION" >> "${HOME}/.gitconfig"
    local gitDirectory="$(readlink -f ~/Repositories)"
    for gitenv in $(find ${gitDirectory} -regextype posix-extended -regex "^${gitDirectory}/.*/\.git-env$"); do
        source "${gitenv}"
        local gitenvDir="$(dirname "${gitenv}")"
        gitUserConfig "${gitenvDir}/.gitconfig" "${GIT_USER_NAME:-$(whoami)}" "${GIT_USER_EMAIL:-$(whoami)@$(hostname)}"
        echo -e "[includeIf \"gitdir:${gitenvDir}/\"]\n\tpath = ${gitenvDir}/.gitconfig" >> "${HOME}/.gitconfig"
    done
    echo "#END CONDITIONAL USER SECTION" >> "${HOME}/.gitconfig"

    log "INFO" "Successfully configured Git"
}

function gitUserConfig () {
    local gitconfig="${1:?"Requires gitconfig file as first parameter!"}"
    local gituser="${2:?"Requires git user as second parameter!"}"
    local gitemail="${3:?"Requires git email as third parameter!"}"

    git config --file "${gitconfig}" user.name "${gituser}"
    git config --file "${gitconfig}" user.email "${gitemail}"

    if [ ! -z $(userSigningKey "${gitemail}") ]; then
        git config --file "${gitconfig}" user.signingkey $(userSigningKey "${gitemail}")
        git config --file "${gitconfig}" commit.gpgsign true
    else
        git config --file "${gitconfig}" --unset user.signingkey || true
        git config --file "${gitconfig}" commit.gpgsign false
    fi
}

function userSigningKey () {
    local gitemail="${1:?"Requires git email as first parameter!"}"
    gpg --with-colons -k "${gitemail}" 2>/dev/null | grep ":s:" | cut -d':' -f5
}

ensure_dotfiles_are_linked
ensure_git_is_configured