#!/usr/bin/env bash
set -e
SCRIPT_DIR="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")"
ROOT_DIR="$(readlink -f "${SCRIPT_DIR}/../..")"
source "${ROOT_DIR}/function.log.sh"
source "${ROOT_DIR}/env.sh"

function ensure_system_resolved_files_are_synced () {
  log "INFO" "Sync /etc/systemd/resolved.conf.d files"
  \cp -f "${SCRIPT_DIR}/system-files/etc/systemd/resolved.conf.d/default.conf" /etc/systemd/resolved.conf.d/
  chmod u+rwX,go+rX /etc/systemd/resolved.conf.d/default.conf
  chown root:root /etc/systemd/resolved.conf.d/default.conf
  systemctl restart systemd-resolved.service
}

if [ -z "${SUDO_USER}" ]; then
  echo "The script must be executed with sudo!"
elif [ -d "${SYSTEM_INSTALLS}" ]; then
  ensure_system_resolved_files_are_synced
fi
