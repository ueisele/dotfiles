#!/usr/bin/env bash
set -e
SCRIPT_DIR="$(dirname $(readlink -f ${BASH_SOURCE[0]}))"
BUILD_DIR="${SCRIPT_DIR}/build"

SOURCE_ISO_FILENAME="Fedora-Silverblue-ostree-x86_64-40-1.14.iso"
TRARGET_ISO_FILENAME="Fedora-Silverblue-kickstart-x86_64-40-1.14.iso"

function download_iso () {
  mkdir -p "${BUILD_DIR}"
  if [ ! -e "${BUILD_DIR}/${SOURCE_ISO_FILENAME}" ]; then
    echo "Downloading ${SOURCE_ISO_FILENAME}"
    local url="https://download.fedoraproject.org/pub/fedora/linux/releases/40/Silverblue/x86_64/iso/${SOURCE_ISO_FILENAME}"
    curl -L -o "${BUILD_DIR}/${SOURCE_ISO_FILENAME}" "${url}"
  fi
}

function build_kickstart_iso () {
  if [ -e "${BUILD_DIR:?}/${TRARGET_ISO_FILENAME:?}" ]; then
    \rm -f "${BUILD_DIR:?}/${TRARGET_ISO_FILENAME:?}"
  fi
  if [ -e "${BUILD_DIR:?}/boot" ]; then
    \rm -rf "${BUILD_DIR:?}/boot"
  fi
  mkdir -p "${BUILD_DIR}/boot"

  xorriso \
    -indev "${BUILD_DIR}/${SOURCE_ISO_FILENAME}" \
    -osirrox on -extract_boot_images "${BUILD_DIR}/boot"
  guestfish -a "${BUILD_DIR}/boot/eltorito_img2_uefi.img" -m /dev/sda --rw copy-in "${SCRIPT_DIR}/grub.cfg" /EFI/BOOT/
  \cp -f "${SCRIPT_DIR}/grub.cfg" "${BUILD_DIR}/BOOT.conf"
  guestfish -a "${BUILD_DIR}/boot/eltorito_img2_uefi.img" -m /dev/sda --rw copy-in "${BUILD_DIR}/BOOT.conf" /EFI/BOOT/

  xorriso  \
    -indev "${BUILD_DIR}/${SOURCE_ISO_FILENAME}" \
    -outdev "${BUILD_DIR}/${TRARGET_ISO_FILENAME}" \
    -compliance no_emul_toc \
    -append_partition 2 0xef "${BUILD_DIR}/boot/eltorito_img2_uefi.img" \
    -boot_image any cat_path=/boot/boot.catalog \
    -boot_image any cat_hidden=on \
    -boot_image any efi_path=--interval:appended_partition_2:all:: \
    -boot_image any platform_id=0xef \
    -boot_image any appended_part_as=gpt \
    -boot_image any partition_offset=16 \
    -update "${SCRIPT_DIR}/grub.cfg" /EFI/BOOT/grub.cfg \
    -update "${SCRIPT_DIR}/grub.conf" /EFI/BOOT/BOOT.conf \
    -update "${SCRIPT_DIR}/grub.cfg" /boot/grub2/grub.cfg \
    -update "${SCRIPT_DIR}/ks.cfg" /ks.cfg
}

download_iso
build_kickstart_iso
