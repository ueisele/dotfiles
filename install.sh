#!/usr/bin/env bash
set -e
SCRIPT_DIR="$(dirname $(readlink -f ${BASH_SOURCE[0]}))"
ROOT_DIR="$(readlink -f ${SCRIPT_DIR})"
source "${ROOT_DIR}/function.log.sh"

function ensure_update_script_is_linked () {
    log "INFO" "Linking update script to ${HOME}"
    ln -sf "${SCRIPT_DIR}/install.sh" "${HOME}/.local/bin/update-all"
}

function ensure_dotfiles_are_installed () {
    for tool in $(find ${ROOT_DIR} -regextype posix-extended -regex "^${ROOT_DIR}/[^_.][^/]*/install\.sh" | sort); do
        log "INFO" "Installing ${tool}"
        ${tool}
    done
}

function ensure_installed () {
    local start_seconds="$(date +%s)"
    ensure_update_script_is_linked
    ensure_dotfiles_are_installed
    local duration_seconds=$(( $(date +%s) - ${start_seconds} ))
    log "INFO" "Installation took ${duration_seconds} seconds."
}

ensure_installed