#!/usr/bin/env bash
set -e
SCRIPT_DIR="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")"
ROOT_DIR="$(readlink -f "${SCRIPT_DIR}/../..")"
source "${ROOT_DIR}/function.log.sh"

function ensure_flatpak_repos_are_enabled () {
  log "INFO" "Ensure additional Flatpak Repositories are enabled"
  flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
}

ensure_flatpak_repos_are_enabled
