#!/usr/bin/env bash
set -e
SCRIPT_DIR="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")"
ROOT_DIR="$(readlink -f "${SCRIPT_DIR}/../..")"
source "${ROOT_DIR}/function.log.sh"

function ensure_flatpak_packages_are_installed () {
  log "INFO" "Ensure Flatpak packages are installed"
  flatpak install --or-update -y fedora com.calibre_ebook.calibre
  flatpak install --or-update -y fedora com.github.tchx84.Flatseal
  flatpak install --or-update -y fedora org.gimp.GIMP
  flatpak install --or-update -y fedora org.inkscape.Inkscape
  flatpak install --or-update -y fedora org.libreoffice.LibreOffice
  flatpak install --or-update -y flathub com.google.Chrome
  flatpak install --or-update -y flathub com.slack.Slack
  flatpak install --or-update -y flathub org.videolan.VLC
  flatpak install --or-update -y flathub com.usebruno.Bruno
}

ensure_flatpak_packages_are_installed
