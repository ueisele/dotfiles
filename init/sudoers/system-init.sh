#!/usr/bin/env bash
set -e
SCRIPT_DIR="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")"
ROOT_DIR="$(readlink -f "${SCRIPT_DIR}/../..")"
source "${ROOT_DIR}/function.log.sh"
source "${ROOT_DIR}/env.sh"

function ensure_sudo_is_configured () {
  log "INFO" "Ensure sudo is configured"
  \cp -f "${SCRIPT_DIR}"/system-files/etc/sudoers /etc/sudoers
  chmod u=r,go= /etc/sudoers
}

ensure_sudo_is_configured
