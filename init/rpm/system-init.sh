#!/usr/bin/env bash
set -e
SCRIPT_DIR="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")"
ROOT_DIR="$(readlink -f "${SCRIPT_DIR}/../..")"
source "${ROOT_DIR}/function.log.sh"

function ensure_yum_repos_are_enabled () {
  log "INFO" "Ensure Yum Repository for VSCode is Enabled"
  sh -c 'echo -e "[code]\nname=Visual Studio Code\nbaseurl=https://packages.microsoft.com/yumrepos/vscode\nenabled=1\ngpgcheck=1\ngpgkey=https://packages.microsoft.com/keys/microsoft.asc" > /etc/yum.repos.d/vscode.repo'

  log "INFO" "Ensure Yum Repository for Beyonde Compare is Enabled"
  sh -c 'echo -e "[scootersoftware]\nname=Scooter Software\nbaseurl=https://www.scootersoftware.com/bcompare4\nenabled=1\ngpgcheck=1\ngpgkey=https://www.scootersoftware.com/RPM-GPG-KEY-scootersoftware" > /etc/yum.repos.d/scootersoftware.repo'
  
  log "INFO" "Ensure Yum Repository RPM Fusion is Enabled"
  rpm-ostree install \
    "https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm" \
    "https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm"
}

ensure_yum_repos_are_enabled
