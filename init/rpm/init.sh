#!/usr/bin/env bash
set -e
SCRIPT_DIR="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")"
ROOT_DIR="$(readlink -f "${SCRIPT_DIR}/../..")"
source "${ROOT_DIR}/function.log.sh"

function ensure_rpm_fusion_is_updated () {
  log "INFO" "Ensure RPM Fusion is updated to release independent package"
  rpm-ostree update \
      --uninstall "rpmfusion-free-release-$(rpm -E %fedora)-1.noarch" \
      --uninstall "rpmfusion-nonfree-release-$(rpm -E %fedora)-1.noarch" \
      --install rpmfusion-free-release \
      --install rpmfusion-nonfree-release
}

function ensure_rpms_are_installed () {
  log "INFO" "Ensure ffmpeg from RPM Fusion is installed"
  rpm-ostree override remove \
    libavdevice-free libavfilter-free libavformat-free ffmpeg-free libpostproc-free libswresample-free libavutil-free libavcodec-free libswscale-free \
    --install ffmpeg
  log "INFO" "Ensure RPMs are installed"
  rpm-ostree install \
    g++ gcc-c++ asciiquarium autoconf automake baobab bcompare bison brotli brotli-devel btop c-ares-devel clamav clamav-update clamd clang cmake code cowsay criu-devel \
    dconf-editor direnv eog evince ffmpeg firewall-config flex gcc git-credential-libsecret glibc-devel glibc-static gnome-calculator gnome-calendar gnome-characters \
    gnome-clocks gnome-connections gnome-contacts gnome-extensions-app gnome-font-viewer gnome-logs gnome-shell-extension-appindicator gnome-shell-extension-dash-to-dock \
    gnome-shell-extension-user-theme gnome-text-editor gnome-themes-extra gnome-tweak-tool gperf gstreamer1-plugin-openh264 gstreamer1-vaapi htop inotify-tools intel-gpu-tools \
    intel-media-driver jansson-devel jemalloc-devel keepassxc krb5-devel lame ldns-utils lftp libarchive-devel libbpf-devel libcap-devel libcurl-devel libev-devel \
    libevent-devel libffi-devel libgsasl-devel libguestfs-tools-c libicu-devel libidn2-devel libpsl-devel libseccomp-devel libssh2 libssh2-devel libtool libuuid-devel \
    libva-utils libvirt-daemon-config-network libvirt-daemon-kvm libxml2-devel libyaml-devel libzstd-devel lld-devel llvm llvm-devel llvm15-devel mc msr-tools nautilus-dropbox \
    neovim net-tools netstat-monitor ninja-build nmap nmstate numix-icon-theme-circle nvme-cli onedrive openldap-clients openldap-devel openssl openssl-devel patch patchelf perf \
    piper pipx podman-compose podmansh protobuf-compiler python3-devel python3-pip python3-pyelftools qemu qemu-kvm qemu-user-static readline-devel \
    setools-console smartmontools sshuttle stoken-cli strace stress-ng sysfsutils systemd-devel telnet tilix tilix-nautilus tmux udica uuid-devel \
    virt-install virt-manager virt-top virt-viewer wireguard-tools yajl-devel yubikey-manager zlib-devel zlib-ng zlib-ng-devel zs
}

ensure_rpm_fusion_is_updated
ensure_rpms_are_installed
