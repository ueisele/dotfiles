#!/usr/bin/env bash
set -e
SCRIPT_DIR="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")"
ROOT_DIR="$(readlink -f "${SCRIPT_DIR}/..")"
source "${ROOT_DIR}/function.log.sh"

function ensure_system_is_initialized () {
    for tool in $(find -L "${SCRIPT_DIR}" -regextype posix-extended -regex "^${SCRIPT_DIR}/[^_.][^/]*/system-(init|install)\.sh" | sort); do
        log "INFO" "Initializing ${tool}"
        ${tool}
    done
}

if [ -z "${SUDO_USER}" ]; then
  echo "The script must be executed with sudo!"
else
  ensure_system_is_initialized
fi
