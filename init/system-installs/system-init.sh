#!/usr/bin/env bash
set -e
SCRIPT_DIR="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")"
ROOT_DIR="$(readlink -f "${SCRIPT_DIR}/../..")"
source "${ROOT_DIR}/function.log.sh"
source "${ROOT_DIR}/env.sh"

function ensure_system_installs_dir_exists () {
  log "INFO" "Ensure system installs dir ${SYSTEM_INSTALLS} exists"
  if [ ! -d "${SYSTEM_INSTALLS}" ]; then
    mkdir -p "${SYSTEM_INSTALLS}"
    mkdir -p "${SYSTEM_INSTALLS_BIN}"
    chown "root:${SUDO_USER}" "${SYSTEM_INSTALLS}"
    chown "root:${SUDO_USER}" "${SYSTEM_INSTALLS_BIN}"
    chmod ug=rwX,o=rX "${SYSTEM_INSTALLS}"
    chmod ug=rwX,o=rX "${SYSTEM_INSTALLS_BIN}"
    setfacl -R -d -m u::rwX "${SYSTEM_INSTALLS}"
    setfacl -R -d -m g::rwX "${SYSTEM_INSTALLS}"
    setfacl -R -d -m o::rX "${SYSTEM_INSTALLS}"
    (semanage fcontext -l | grep "${SYSTEM_INSTALLS}") || semanage fcontext -a -t bin_t "${SYSTEM_INSTALLS}(/.*)*/bin(/.*)?"
    restorecon -RvF "${SYSTEM_INSTALLS}"
  fi
}

function ensure_system_profile_files_exists () {
  log "INFO" "Ensure system installs profile file exists in /etc/profile.d/"
  \cp -f "${SCRIPT_DIR}/system-files/etc/profile.d/system-installs.sh" /etc/profile.d/
  chmod u+rwX,go+rX /etc/profile.d/system-installs.sh
  chown root:root /etc/profile.d/system-installs.sh
}

if [ -z "${SUDO_USER}" ]; then
  echo "The script must be executed with sudo!"
else
  ensure_system_installs_dir_exists
  ensure_system_profile_files_exists
fi
