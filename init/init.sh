#!/usr/bin/env bash
set -e
SCRIPT_DIR="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")"
ROOT_DIR="$(readlink -f "${SCRIPT_DIR}/..")"
source "${ROOT_DIR}/function.log.sh"

function ensure_initialized () {
    for tool in $(find -L "${SCRIPT_DIR}" -regextype posix-extended -regex "^${SCRIPT_DIR}/[^_.][^/]*/(init|install)\.sh" | sort); do
        log "INFO" "Initializing ${tool}"
        ${tool}
    done
}

ensure_initialized
