#!/usr/bin/env bash
set -e
SCRIPT_DIR="$(dirname $(readlink -f ${BASH_SOURCE[0]}))"
ROOT_DIR="$(readlink -f ${SCRIPT_DIR}/..)"
source ${ROOT_DIR}/function.log.sh

REPO_DIR="${SCRIPT_DIR}/repos"
mkdir -p "${REPO_DIR}"

function ensure_install_script_is_linked () {
  log "INFO" "Linking Rust update script to ${HOME}"
  ln -sf "${SCRIPT_DIR}/install.sh" "${HOME}/.local/bin/update-rust"
}

function ensure_rust_is_installed_and_updated () {
  # https://www.rust-lang.org/tools/install
  if [ ! -d "${HOME}/.cargo" ]; then
    log "INFO" "Installing Rust to ${HOME}/.cargo"
    curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- -y --no-modify-path
    export PATH="${HOME}/.cargo/bin:$PATH"
  else
    log "INFO" "Updating Rust in ${HOME}/.cargo"
    rustup default stable
    rustup update
  fi
  mkdir -p ${HOME}/.zsh/zfunc.d/
  rustup completions zsh rustup > ${HOME}/.zsh/zfunc.d/_rustup
  rustup completions zsh cargo > ${HOME}/.zsh/zfunc.d/_cargo
}

function ensure_dotfiles_are_linked () {
  log "INFO" "Linking Rust dotfiles to ${HOME}"
  ${ROOT_DIR}/tool.link-files.sh "${SCRIPT_DIR}/files"
}

function ensure_rust_targets_are_added () {
  log "INFO" "Add Rust Compile Targets"
  # Wasm
  rustup target add wasm32-unknown-unknown
  rustup target add wasm32-unknown-emscripten
  rustup target add wasm32-wasip1 wasm32-wasip1-threads
  rustup target add wasm32-wasip2
}

ensure_install_script_is_linked
ensure_rust_is_installed_and_updated
ensure_dotfiles_are_linked
ensure_rust_targets_are_added
