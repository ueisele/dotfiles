#
# Configures Rust
#

# Return if requirements are not found
if [ ! -f "${HOME}/.cargo/bin/rustup" ]; then
    return 1
fi

## Path
path=(
    ${HOME}/.cargo/bin
    $path
  )
