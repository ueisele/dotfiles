#!/usr/bin/env bash
set -e

function init_yubikeys () {
  echo "Initialize SSH"
  (
    mkdir -p ~/.ssh-yubikey
    chmod u=rwX,go= ~/.ssh-yubikey
    cd ~/.ssh-yubikey
    while IFS= read -n1 -r -p "add ssh key of another yubikey [y]es|[n]o: " && [[ $REPLY == y ]]; do
      printf "\n"
      ssh-keygen -K -N ''
    done
  )
  printf "\n"
  (
    mkdir -p ~/.ssh
    chmod u=rwX,go= ~/.ssh
    cd ~/.ssh
    while IFS= read -n1 -r -p "add yubikey which should be used to clone git repositories [y]es: " && [[ $REPLY != y ]]; do
      printf "\n"
    done
    printf "\n"
    ssh-keygen -K -N ''
  )
}

function clone_git_repos () {
  echo "Clone Git Repositories"
  ssh-keyscan gitlab.com >> ~/.ssh/known_hosts
  local private_key_file="$(find ~/.ssh -name "id_ed25519_sk_rk_yubikey-*" ! -name "*.pub")"
  export GIT_SSH_COMMAND="ssh -i ${private_key_file} -o IdentitiesOnly=yes"
  git clone git@gitlab.com:ueisele/dotfiles.git ~/.dotfiles
  git clone git@gitlab.com:ueisele/secret-dotfiles.git ~/.secret-dotfiles
}

function print_next_steps () {
  printf "\n"
  cat ~/.dotfiles/README.adoc
}

init_yubikeys
clone_git_repos
print_next_steps
